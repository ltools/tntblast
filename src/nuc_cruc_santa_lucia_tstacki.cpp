param_loop_terminal_H[AT][AA] = -3.20f;
param_loop_terminal_S[AT][AA] = -0.00806061583104949f;
param_loop_terminal_H[AA][TA] = -3.20f;
param_loop_terminal_S[AA][TA] = -0.00806061583104949f;

param_loop_terminal_H[AT][AC] = -0.90f;
param_loop_terminal_S[AT][AC] = -0.00193454779945188f;
param_loop_terminal_H[CA][TA] = -0.90f;
param_loop_terminal_S[CA][TA] = -0.00193454779945188f;

param_loop_terminal_H[AT][AG] = -2.30f;
param_loop_terminal_S[AT][AG] = -0.00580364339835563f;
param_loop_terminal_H[GA][TA] = -2.30f;
param_loop_terminal_S[GA][TA] = -0.00580364339835563f;

param_loop_terminal_H[AT][AT] = 0.00f;
param_loop_terminal_S[AT][AT] = 0.00f;
param_loop_terminal_H[TA][TA] = 0.00f;
param_loop_terminal_S[TA][TA] = 0.00f;

param_loop_terminal_H[AT][CA] = -2.20f;
param_loop_terminal_S[AT][CA] = -0.00515879413187168f;
param_loop_terminal_H[AC][TA] = -2.20f;
param_loop_terminal_S[AC][TA] = -0.00515879413187168f;

param_loop_terminal_H[AT][CC] = -0.50f;
param_loop_terminal_S[AT][CC] = -0.000967273899725939f;
param_loop_terminal_H[CC][TA] = -0.50f;
param_loop_terminal_S[CC][TA] = -0.000967273899725939f;

param_loop_terminal_H[AT][CG] = 0.00f;
param_loop_terminal_S[AT][CG] = 0.00f;
param_loop_terminal_H[GC][TA] = 0.00f;
param_loop_terminal_S[GC][TA] = 0.00f;

param_loop_terminal_H[AT][CT] = -1.20f;
param_loop_terminal_S[AT][CT] = -0.00290182169917782f;
param_loop_terminal_H[TC][TA] = -1.20f;
param_loop_terminal_S[TC][TA] = -0.00290182169917782f;

param_loop_terminal_H[AT][GA] = -2.70f;
param_loop_terminal_S[AT][GA] = -0.00677091729808157f;
param_loop_terminal_H[AG][TA] = -2.70f;
param_loop_terminal_S[AG][TA] = -0.00677091729808157f;

param_loop_terminal_H[AT][GC] = 0.00f;
param_loop_terminal_S[AT][GC] = 0.00f;
param_loop_terminal_H[CG][TA] = 0.00f;
param_loop_terminal_S[CG][TA] = 0.00f;

param_loop_terminal_H[AT][GG] = -1.30f;
param_loop_terminal_S[AT][GG] = -0.00290182169917782f;
param_loop_terminal_H[GG][TA] = -1.30f;
param_loop_terminal_S[GG][TA] = -0.00290182169917782f;

param_loop_terminal_H[AT][GT] = -2.90f;
param_loop_terminal_S[AT][GT] = -0.00773819119780751f;
param_loop_terminal_H[TG][TA] = -2.90f;
param_loop_terminal_S[TG][TA] = -0.00773819119780751f;

param_loop_terminal_H[AT][TA] = 0.00f;
param_loop_terminal_S[AT][TA] = 0.00f;
param_loop_terminal_H[AT][TA] = 0.00f;
param_loop_terminal_S[AT][TA] = 0.00f;

param_loop_terminal_H[AT][TC] = -2.80f;
param_loop_terminal_S[AT][TC] = -0.00806061583104949f;
param_loop_terminal_H[CT][TA] = -2.80f;
param_loop_terminal_S[CT][TA] = -0.00806061583104949f;

param_loop_terminal_H[AT][TG] = -3.50f;
param_loop_terminal_S[AT][TG] = -0.00967273899725939f;
param_loop_terminal_H[GT][TA] = -3.50f;
param_loop_terminal_S[GT][TA] = -0.00967273899725939f;

param_loop_terminal_H[AT][TT] = -2.40f;
param_loop_terminal_S[AT][TT] = -0.00644849266483959f;
param_loop_terminal_H[TT][TA] = -2.40f;
param_loop_terminal_S[TT][TA] = -0.00644849266483959f;

param_loop_terminal_H[CG][AA] = -2.80f;
param_loop_terminal_S[CG][AA] = -0.00580364339835563f;
param_loop_terminal_H[AA][GC] = -2.80f;
param_loop_terminal_S[AA][GC] = -0.00580364339835563f;

param_loop_terminal_H[CG][AC] = -2.00f;
param_loop_terminal_S[CG][AC] = -0.00386909559890376f;
param_loop_terminal_H[CA][GC] = -2.00f;
param_loop_terminal_S[CA][GC] = -0.00386909559890376f;

param_loop_terminal_H[CG][AG] = -3.00f;
param_loop_terminal_S[CG][AG] = -0.00677091729808157f;
param_loop_terminal_H[GA][GC] = -3.00f;
param_loop_terminal_S[GA][GC] = -0.00677091729808157f;

param_loop_terminal_H[CG][AT] = 0.00f;
param_loop_terminal_S[CG][AT] = 0.00f;
param_loop_terminal_H[TA][GC] = 0.00f;
param_loop_terminal_S[TA][GC] = 0.00f;

param_loop_terminal_H[CG][CA] = -2.40f;
param_loop_terminal_S[CG][CA] = -0.00515879413187167f;
param_loop_terminal_H[AC][GC] = -2.40f;
param_loop_terminal_S[AC][GC] = -0.00515879413187167f;

param_loop_terminal_H[CG][CC] = -1.40f;
param_loop_terminal_S[CG][CC] = -0.00290182169917782f;
param_loop_terminal_H[CC][GC] = -1.40f;
param_loop_terminal_S[CC][GC] = -0.00290182169917782f;

param_loop_terminal_H[CG][CG] = 0.00f;
param_loop_terminal_S[CG][CG] = 0.00f;
param_loop_terminal_H[GC][GC] = 0.00f;
param_loop_terminal_S[GC][GC] = 0.00f;

param_loop_terminal_H[CG][CT] = -2.40f;
param_loop_terminal_S[CG][CT] = -0.00548121876511366f;
param_loop_terminal_H[TC][GC] = -2.40f;
param_loop_terminal_S[TC][GC] = -0.00548121876511366f;

param_loop_terminal_H[CG][GA] = -5.10f;
param_loop_terminal_S[CG][GA] = -0.0132194099629212f;
param_loop_terminal_H[AG][GC] = -5.10f;
param_loop_terminal_S[AG][GC] = -0.0132194099629212f;

param_loop_terminal_H[CG][GC] = 0.00f;
param_loop_terminal_S[CG][GC] = 0.00f;
param_loop_terminal_H[CG][GC] = 0.00f;
param_loop_terminal_S[CG][GC] = 0.00f;

param_loop_terminal_H[CG][GG] = -2.90f;
param_loop_terminal_S[CG][GG] = -0.00644849266483959f;
param_loop_terminal_H[GG][GC] = -2.90f;
param_loop_terminal_S[GG][GC] = -0.00644849266483959f;

param_loop_terminal_H[CG][GT] = -2.90f;
param_loop_terminal_S[CG][GT] = -0.00612606803159761f;
param_loop_terminal_H[TG][GC] = -2.90f;
param_loop_terminal_S[TG][GC] = -0.00612606803159761f;

param_loop_terminal_H[CG][TC] = -3.10f;
param_loop_terminal_S[CG][TC] = -0.00806061583104949f;
param_loop_terminal_H[CT][GC] = -3.10f;
param_loop_terminal_S[CT][GC] = -0.00806061583104949f;

param_loop_terminal_H[CG][TG] = -5.90f;
param_loop_terminal_S[CG][TG] = -0.016121231662099f;
param_loop_terminal_H[GT][GC] = -5.90f;
param_loop_terminal_S[GT][GC] = -0.016121231662099f;

param_loop_terminal_H[CG][TT] = -5.30f;
param_loop_terminal_S[CG][TT] = -0.0141866838626471f;
param_loop_terminal_H[TT][GC] = -5.30f;
param_loop_terminal_S[TT][GC] = -0.0141866838626471f;

param_loop_terminal_H[GC][AA] = -6.00f;
param_loop_terminal_S[GC][AA] = -0.016121231662099f;
param_loop_terminal_H[AA][CG] = -6.00f;
param_loop_terminal_S[AA][CG] = -0.016121231662099f;

param_loop_terminal_H[GC][AC] = -4.00f;
param_loop_terminal_S[GC][AC] = -0.0106400128969853f;
param_loop_terminal_H[CA][CG] = -4.00f;
param_loop_terminal_S[CA][CG] = -0.0106400128969853f;

param_loop_terminal_H[GC][AG] = -3.40f;
param_loop_terminal_S[GC][AG] = -0.00838304046429147f;
param_loop_terminal_H[GA][CG] = -3.40f;
param_loop_terminal_S[GA][CG] = -0.00838304046429147f;

param_loop_terminal_H[GC][AT] = 0.00f;
param_loop_terminal_S[GC][AT] = 0.00f;
param_loop_terminal_H[TA][CG] = 0.00f;
param_loop_terminal_S[TA][CG] = 0.00f;

param_loop_terminal_H[GT][AA] = 0.00f;
param_loop_terminal_S[GT][AA] = 0.0016121231662099f;
param_loop_terminal_H[AA][TG] = 0.00f;
param_loop_terminal_S[AA][TG] = 0.0016121231662099f;

param_loop_terminal_H[GT][AC] = 0.00f;
param_loop_terminal_S[GT][AC] = 0.000644849266483959f;
param_loop_terminal_H[CA][TG] = 0.00f;
param_loop_terminal_S[CA][TG] = 0.000644849266483959f;

param_loop_terminal_H[GT][AG] = 0.00f;
param_loop_terminal_S[GT][AG] = 0.0016121231662099f;
param_loop_terminal_H[GA][TG] = 0.00f;
param_loop_terminal_S[GA][TG] = 0.0016121231662099f;

param_loop_terminal_H[GT][AT] = -3.70f;
param_loop_terminal_S[GT][AT] = -0.00999516363050137f;
param_loop_terminal_H[TA][TG] = -3.70f;
param_loop_terminal_S[TA][TG] = -0.00999516363050137f;

param_loop_terminal_H[GC][CA] = -2.80f;
param_loop_terminal_S[GC][CA] = -0.00580364339835563f;
param_loop_terminal_H[AC][CG] = -2.80f;
param_loop_terminal_S[AC][CG] = -0.00580364339835563f;

param_loop_terminal_H[GC][CC] = -2.50f;
param_loop_terminal_S[GC][CC] = -0.00612606803159761f;
param_loop_terminal_H[CC][CG] = -2.50f;
param_loop_terminal_S[CC][CG] = -0.00612606803159761f;

param_loop_terminal_H[GC][CG] = 0.00f;
param_loop_terminal_S[GC][CG] = 0.00f;
param_loop_terminal_H[GC][CG] = 0.00f;
param_loop_terminal_S[GC][CG] = 0.00f;

param_loop_terminal_H[GC][CT] = -3.30f;
param_loop_terminal_S[GC][CT] = -0.00838304046429147f;
param_loop_terminal_H[TC][CG] = -3.30f;
param_loop_terminal_S[TC][CG] = -0.00838304046429147f;

param_loop_terminal_H[GT][CA] = 0.00f;
param_loop_terminal_S[GT][CA] = 0.000644849266483959f;
param_loop_terminal_H[AC][TG] = 0.00f;
param_loop_terminal_S[AC][TG] = 0.000644849266483959f;

param_loop_terminal_H[GT][CC] = 0.00f;
param_loop_terminal_S[GT][CC] = 0.000644849266483959f;
param_loop_terminal_H[CC][TG] = 0.00f;
param_loop_terminal_S[CC][TG] = 0.000644849266483959f;

param_loop_terminal_H[GT][CG] = -4.50f;
param_loop_terminal_S[GT][CG] = -0.0116072867967113f;
param_loop_terminal_H[GC][TG] = -4.50f;
param_loop_terminal_S[GC][TG] = -0.0116072867967113f;

param_loop_terminal_H[GT][CT] = 0.00f;
param_loop_terminal_S[GT][CT] = 0.000644849266483959f;
param_loop_terminal_H[TC][TG] = 0.00f;
param_loop_terminal_S[TC][TG] = 0.000644849266483959f;

param_loop_terminal_H[GC][GA] = -4.00f;
param_loop_terminal_S[GC][GA] = -0.00967273899725939f;
param_loop_terminal_H[AG][CG] = -4.00f;
param_loop_terminal_S[AG][CG] = -0.00967273899725939f;

param_loop_terminal_H[GC][GC] = 0.00f;
param_loop_terminal_S[GC][GC] = 0.00f;
param_loop_terminal_H[CG][CG] = 0.00f;
param_loop_terminal_S[CG][CG] = 0.00f;

param_loop_terminal_H[GC][GG] = -5.30f;
param_loop_terminal_S[GC][GG] = -0.0138642592294051f;
param_loop_terminal_H[GG][CG] = -5.30f;
param_loop_terminal_S[GG][CG] = -0.0138642592294051f;

param_loop_terminal_H[GC][GT] = -3.70f;
param_loop_terminal_S[GC][GT] = -0.00935031436401741f;
param_loop_terminal_H[TG][CG] = -3.70f;
param_loop_terminal_S[TG][CG] = -0.00935031436401741f;

param_loop_terminal_H[GT][GA] = 0.00f;
param_loop_terminal_S[GT][GA] = 0.0016121231662099f;
param_loop_terminal_H[AG][TG] = 0.00f;
param_loop_terminal_S[AG][TG] = 0.0016121231662099f;

param_loop_terminal_H[GT][GC] = -5.90f;
param_loop_terminal_S[GT][GC] = -0.016121231662099f;
param_loop_terminal_H[CG][TG] = -5.90f;
param_loop_terminal_S[CG][TG] = -0.016121231662099f;

param_loop_terminal_H[GT][GG] = 0.00f;
param_loop_terminal_S[GT][GG] = 0.0016121231662099f;
param_loop_terminal_H[GG][TG] = 0.00f;
param_loop_terminal_S[GG][TG] = 0.0016121231662099f;

param_loop_terminal_H[GT][GT] = -2.00f;
param_loop_terminal_S[GT][GT] = -0.0048363694986297f;
param_loop_terminal_H[TG][TG] = -2.00f;
param_loop_terminal_S[TG][TG] = -0.0048363694986297f;

param_loop_terminal_H[GC][TC] = -2.50f;
param_loop_terminal_S[GC][TC] = -0.00612606803159761f;
param_loop_terminal_H[CT][CG] = -2.50f;
param_loop_terminal_S[CT][CG] = -0.00612606803159761f;

param_loop_terminal_H[GC][TG] = -4.50f;
param_loop_terminal_S[GC][TG] = -0.0116072867967113f;
param_loop_terminal_H[GT][CG] = -4.50f;
param_loop_terminal_S[GT][CG] = -0.0116072867967113f;

param_loop_terminal_H[GC][TT] = -6.10f;
param_loop_terminal_S[GC][TT] = -0.0167660809285829f;
param_loop_terminal_H[TT][CG] = -6.10f;
param_loop_terminal_S[TT][CG] = -0.0167660809285829f;

param_loop_terminal_H[GT][TA] = -3.50f;
param_loop_terminal_S[GT][TA] = -0.00967273899725939f;
param_loop_terminal_H[AT][TG] = -3.50f;
param_loop_terminal_S[AT][TG] = -0.00967273899725939f;

param_loop_terminal_H[GT][TC] = 0.00f;
param_loop_terminal_S[GT][TC] = 0.000644849266483959f;
param_loop_terminal_H[CT][TG] = 0.00f;
param_loop_terminal_S[CT][TG] = 0.000644849266483959f;

param_loop_terminal_H[GT][TG] = -2.00f;
param_loop_terminal_S[GT][TG] = -0.0048363694986297f;
param_loop_terminal_H[GT][TG] = -2.00f;
param_loop_terminal_S[GT][TG] = -0.0048363694986297f;

param_loop_terminal_H[GT][TT] = 0.00f;
param_loop_terminal_S[GT][TT] = 0.000644849266483959f;
param_loop_terminal_H[TT][TG] = 0.00f;
param_loop_terminal_S[TT][TG] = 0.000644849266483959f;

param_loop_terminal_H[TA][AA] = -2.70f;
param_loop_terminal_S[TA][AA] = -0.00677091729808157f;
param_loop_terminal_H[AA][AT] = -2.70f;
param_loop_terminal_S[AA][AT] = -0.00677091729808157f;

param_loop_terminal_H[TA][AC] = -2.60f;
param_loop_terminal_S[TA][AC] = -0.00709334193132355f;
param_loop_terminal_H[CA][AT] = -2.60f;
param_loop_terminal_S[CA][AT] = -0.00709334193132355f;

param_loop_terminal_H[TA][AG] = -2.40f;
param_loop_terminal_S[TA][AG] = -0.00612606803159761f;
param_loop_terminal_H[GA][AT] = -2.40f;
param_loop_terminal_S[GA][AT] = -0.00612606803159761f;

param_loop_terminal_H[TA][AT] = 0.00f;
param_loop_terminal_S[TA][AT] = 0.00f;
param_loop_terminal_H[TA][AT] = 0.00f;
param_loop_terminal_S[TA][AT] = 0.00f;

param_loop_terminal_H[TG][AA] = 0.00f;
param_loop_terminal_S[TG][AA] = 0.0016121231662099f;
param_loop_terminal_H[AA][GT] = 0.00f;
param_loop_terminal_S[AA][GT] = 0.0016121231662099f;

param_loop_terminal_H[TG][AC] = 0.00f;
param_loop_terminal_S[TG][AC] = 0.000644849266483959f;
param_loop_terminal_H[CA][GT] = 0.00f;
param_loop_terminal_S[CA][GT] = 0.000644849266483959f;

param_loop_terminal_H[TG][AG] = 0.00f;
param_loop_terminal_S[TG][AG] = 0.0016121231662099f;
param_loop_terminal_H[GA][GT] = 0.00f;
param_loop_terminal_S[GA][GT] = 0.0016121231662099f;

param_loop_terminal_H[TG][AT] = -2.30f;
param_loop_terminal_S[TG][AT] = -0.00580364339835563f;
param_loop_terminal_H[TA][GT] = -2.30f;
param_loop_terminal_S[TA][GT] = -0.00580364339835563f;

param_loop_terminal_H[TA][CA] = -2.60f;
param_loop_terminal_S[TA][CA] = -0.00677091729808157f;
param_loop_terminal_H[AC][AT] = -2.60f;
param_loop_terminal_S[AC][AT] = -0.00677091729808157f;

param_loop_terminal_H[TA][CC] = -0.50f;
param_loop_terminal_S[TA][CC] = -0.000967273899725939f;
param_loop_terminal_H[CC][AT] = -0.50f;
param_loop_terminal_S[CC][AT] = -0.000967273899725939f;

param_loop_terminal_H[TA][CT] = -2.70f;
param_loop_terminal_S[TA][CT] = -0.00709334193132355f;
param_loop_terminal_H[TC][AT] = -2.70f;
param_loop_terminal_S[TC][AT] = -0.00709334193132355f;

param_loop_terminal_H[TG][CA] = 0.00f;
param_loop_terminal_S[TG][CA] = 0.000644849266483959f;
param_loop_terminal_H[AC][GT] = 0.00f;
param_loop_terminal_S[AC][GT] = 0.000644849266483959f;

param_loop_terminal_H[TG][CC] = 0.00f;
param_loop_terminal_S[TG][CC] = 0.000644849266483959f;
param_loop_terminal_H[CC][GT] = 0.00f;
param_loop_terminal_S[CC][GT] = 0.000644849266483959f;

param_loop_terminal_H[TG][CG] = -3.70f;
param_loop_terminal_S[TG][CG] = -0.00935031436401741f;
param_loop_terminal_H[GC][GT] = -3.70f;
param_loop_terminal_S[GC][GT] = -0.00935031436401741f;

param_loop_terminal_H[TG][CT] = 0.00f;
param_loop_terminal_S[TG][CT] = 0.000644849266483959f;
param_loop_terminal_H[TC][GT] = 0.00f;
param_loop_terminal_S[TC][GT] = 0.000644849266483959f;

param_loop_terminal_H[TA][GA] = -1.90f;
param_loop_terminal_S[TA][GA] = -0.00419152023214574f;
param_loop_terminal_H[AG][AT] = -1.90f;
param_loop_terminal_S[AG][AT] = -0.00419152023214574f;

param_loop_terminal_H[TA][GG] = -1.50f;
param_loop_terminal_S[TA][GG] = -0.00354667096566178f;
param_loop_terminal_H[GG][AT] = -1.50f;
param_loop_terminal_S[GG][AT] = -0.00354667096566178f;

param_loop_terminal_H[TA][GT] = -2.30f;
param_loop_terminal_S[TA][GT] = -0.00580364339835563f;
param_loop_terminal_H[TG][AT] = -2.30f;
param_loop_terminal_S[TG][AT] = -0.00580364339835563f;

param_loop_terminal_H[TG][GA] = 0.00f;
param_loop_terminal_S[TG][GA] = 0.0016121231662099f;
param_loop_terminal_H[AG][GT] = 0.00f;
param_loop_terminal_S[AG][GT] = 0.0016121231662099f;

param_loop_terminal_H[TG][GC] = -2.90f;
param_loop_terminal_S[TG][GC] = -0.00612606803159761f;
param_loop_terminal_H[CG][GT] = -2.90f;
param_loop_terminal_S[CG][GT] = -0.00612606803159761f;

param_loop_terminal_H[TG][GG] = 0.00f;
param_loop_terminal_S[TG][GG] = 0.0016121231662099f;
param_loop_terminal_H[GG][GT] = 0.00f;
param_loop_terminal_S[GG][GT] = 0.0016121231662099f;

param_loop_terminal_H[TG][GT] = -2.00f;
param_loop_terminal_S[TG][GT] = -0.0048363694986297f;
param_loop_terminal_H[TG][GT] = -2.00f;
param_loop_terminal_S[TG][GT] = -0.0048363694986297f;

param_loop_terminal_H[TA][TC] = -1.40f;
param_loop_terminal_S[TA][TC] = -0.00354667096566178f;
param_loop_terminal_H[CT][AT] = -1.40f;
param_loop_terminal_S[CT][AT] = -0.00354667096566178f;

param_loop_terminal_H[TA][TG] = -3.70f;
param_loop_terminal_S[TA][TG] = -0.00999516363050137f;
param_loop_terminal_H[GT][AT] = -3.70f;
param_loop_terminal_S[GT][AT] = -0.00999516363050137f;

param_loop_terminal_H[TA][TT] = -2.30f;
param_loop_terminal_S[TA][TT] = -0.00644849266483959f;
param_loop_terminal_H[TT][AT] = -2.30f;
param_loop_terminal_S[TT][AT] = -0.00644849266483959f;

param_loop_terminal_H[TG][TA] = -2.90f;
param_loop_terminal_S[TG][TA] = -0.00773819119780751f;
param_loop_terminal_H[AT][GT] = -2.90f;
param_loop_terminal_S[AT][GT] = -0.00773819119780751f;

param_loop_terminal_H[TG][TC] = 0.00f;
param_loop_terminal_S[TG][TC] = 0.000644849266483959f;
param_loop_terminal_H[CT][GT] = 0.00f;
param_loop_terminal_S[CT][GT] = 0.000644849266483959f;

param_loop_terminal_H[TG][TG] = -2.00f;
param_loop_terminal_S[TG][TG] = -0.0048363694986297f;
param_loop_terminal_H[GT][GT] = -2.00f;
param_loop_terminal_S[GT][GT] = -0.0048363694986297f;

param_loop_terminal_H[TG][TT] = 0.00f;
param_loop_terminal_S[TG][TT] = 0.000644849266483959f;
param_loop_terminal_H[TT][GT] = 0.00f;
param_loop_terminal_S[TT][GT] = 0.000644849266483959f;

