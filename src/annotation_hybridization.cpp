// This program was prepared by the Regents of the University of California at 
// Los Alamos National Laboratory (the University) under contract No. W-7405-ENG-36 
// with the U.S. Department of Energy (DOE). All rights in the program are reserved 
// by the DOE and the University.  Permission is granted to the public to copy and 
// use this software without charge, provided that this Notice and any statement of 
// authorship are reproduced on all copies.  Neither the U.S. Government nor the 
// University makes any warranty, express or implied, or assumes any liability or 
// responsibility for the use of this software.

#include "stdafx.h"
#include "annotation.h"
#include "nuc_cruc.h"

#include <sstream>

using namespace std;

// A structure for storing primer info
struct PrimerInfo {
	enum {PRIMER_1, PRIMER_2};

	PrimerInfo()
	{
		start = 0;
		tm = 0.0f;
		clamp3 = 0;
		clamp5 = 0;
		id = PRIMER_1;
		
		num_match = 0;
		len = 0;
	};

	PrimerInfo(const unsigned int &m_start, const float &m_tm, const unsigned int &m_clamp,
		const unsigned int &m_num_match, const unsigned int &m_len)
	{
		start = m_start;
		tm = m_tm;
		clamp3 = m_clamp;
		
		num_match = m_num_match;
		len = m_len;
	};
	
	// Which primer does this record refere to?
	unsigned int id;

	// Starting location
	unsigned int start;

	// Melting temperature
	float tm;

	// 3' clamp length (i.e. number of bases of exact match at the 3' end)
	unsigned int clamp3;

	// 5' clamp length (i.e. number of bases of exact match at the 5' end)
	unsigned int clamp5;
	
	// The number of Watson and Crick base pairs
	unsigned int num_match;
	
	// The length of the primer sequence
	unsigned int len;
};

// Here are all of the possible binding configurations for primers P1 and P2
// against a target sequence:
//
//                             3'-P2-5'
//	5'- =============================== -3'
//	3'- =============================== -5'
//         5'-P1-3'
//
//                             3'-P1-5'
//	5'- =============================== -3'
//	3'- =============================== -5'
//         5'-P2-3'
//
//                             3'-P1-5'
//	5'- =============================== -3'		* single primer amplification
//	3'- =============================== -5'
//         5'-P1-3'
//
//                             3'-P2-5'
//	5'- =============================== -3'		* single primer amplification
//	3'- =============================== -5'
//         5'-P2-3'
//
std::list< std::list<GeneAnnotation>::iterator > DNAMol::find_primers(const DNA3Seq &m_primer_1, 
		const DNA3Seq &m_primer_2, const float &m_salt, const float &m_strand, 
		const float &m_tm, const unsigned int &m_exact_match, 
		const unsigned int &m_min_amplicon_len, 
		const unsigned int &m_max_amplicon_len, const float &m_identity,
		std::list<SeqHit> &hit_list)
{

	list< list<GeneAnnotation>::iterator > annot_iter_list;
	
	// Check BOTH primer 1 and primer 2 against the minus
	// strand of the target sequence

	// Save the locations of all of the places that primer 1 and 2 bind to.
	list<PrimerInfo> primer1_bind_minus;
	list<PrimerInfo> primer2_bind_minus;


	// Compute the Tm for the plus and minus strands at the same time!
	NucCruc melt_1;		// P1 binds to the minus strand?
	NucCruc melt_2;		// P2 binds to the minus strand?

	melt_1.Salt(m_salt);
	melt_2.Salt(m_salt);

	melt_1.Strand(m_strand);
	melt_2.Strand(m_strand);

	melt_1.IdentityCutoff(m_identity);
	melt_2.IdentityCutoff(m_identity);

	// Convert the DNA sequences of the primers into
	// strings for use in the Nucleic Crucible engine
	const string primer1 = m_primer_1.str();
	const string primer2 = m_primer_2.str();
	
	// Load the first primer seq as the reference
	melt_1.reference(primer1);
	melt_2.reference(primer2);

	DNA3Seq::iterator iter = seq.begin();
	
	const unsigned int primer_1_len = primer1.size();
	const unsigned int primer_2_len = primer2.size();

	// If this is a circular molecule, make len large enought to accomodate the
	// primer wrapping around.
	unsigned int len = (is_circular) ? seq.size() + max(primer_1_len, primer_2_len) - 1 : seq.size();
	
	// For non-periodic molecules, we need to allowed a primer to overhang the sequence.
	// Hence "Padding" sequences for appending and prepending to the target sequence
	const int pad_len = (is_circular) ? 0 : max(primer_1_len, primer_2_len);
	
	for(int i = -pad_len;i < ((int)len + pad_len);i++){

		if((i < 0) || (iter == seq.end())){
			// Add pre- and post-padding to the target sequence
			melt_1.push_back_target_native(NucCruc::GAP, false);
			melt_2.push_back_target_native(NucCruc::GAP, false);				
		}
		else{
			// Bind to the minus strand of the target DNA
			switch(iter.base()){
				case DNA3Seq::A:
					melt_1.push_back_target_native(NucCruc::A, true);
					melt_2.push_back_target_native(NucCruc::A, true);
					break;
				case DNA3Seq::T:
					melt_1.push_back_target_native(NucCruc::T, true);
					melt_2.push_back_target_native(NucCruc::T, true);
					break;
				case DNA3Seq::G:
					melt_1.push_back_target_native(NucCruc::G, true);
					melt_2.push_back_target_native(NucCruc::G, true);
					break;
				case DNA3Seq::C:
					melt_1.push_back_target_native(NucCruc::C, true);
					melt_2.push_back_target_native(NucCruc::C, true);
					break;
				default:
					// We have encountered a gap or unknown base
					melt_1.clear_target();
					melt_2.clear_target();
					break;
			};
			
			iter++;
			
			if(is_circular && (iter == seq.end()) ){
				iter = seq.begin();
			}
		}
		
		// Do we have enough bases to compare for primer 1
		if(melt_1.size_target() == primer_1_len){
		
			// Compute the melting temperatures
			// Tm for the plus strand
			float tm;

			if( melt_1.anchor_reference(m_exact_match) && 
			  ( ( tm = melt_1.Tm_reference()) >= m_tm) ){

				// Store the location and Tm for this primer
				primer1_bind_minus.push_back( 
					PrimerInfo( i, tm, melt_1.anchor_reference(), 
						melt_1.num_identity_reference(), primer_1_len) );
			}

			melt_1.pop_front_target();
		}
		
		// Do we have enough bases to compare for primer 2
		if(melt_2.size_target() == primer_2_len){
		
			// Compute the melting temperatures
			// Tm for the plus strand
			float tm;

			if( melt_2.anchor_reference(m_exact_match) && 
			  ( ( tm = melt_2.Tm_reference() ) >= m_tm)){
				
				// Store the location and Tm for this primer
				primer2_bind_minus.push_back( 
					PrimerInfo(i, tm, melt_2.anchor_reference(),
					melt_2.num_identity_reference(), primer_2_len) );
			}

			melt_2.pop_front_target();
		}
	}	
	
	// Test the possible primer arrangments that can produce an amplicon.
	list<PrimerInfo>::const_iterator bind_iter;

	for(bind_iter = primer1_bind_minus.begin();bind_iter != primer1_bind_minus.end();bind_iter++){

		// Clear any existing sequence in the melting buffers
		melt_1.clear_target();
		melt_2.clear_target();

		// If this is a circular molecule, make len large enought to accomodate the
		// primer wrapping around.
		const int len = (is_circular) ? m_max_amplicon_len : 
			min( (int)m_max_amplicon_len, (int)seq.size() + pad_len - (int)bind_iter->start);

		//iter = seq.begin() + bind_iter->first;
		iter = (is_circular) ?  seq.begin() + wrap(bind_iter->start, seq.size()) :
				seq.begin() + max(0, (int)bind_iter->start);
		
		int base_pos = max(0, (int)bind_iter->start);
		
		for(int i =  min(0, (int)bind_iter->start);i < len;i++, base_pos++){
			
			// Bind to the plus strand of the target DNA
			if((i < 0) || (iter == seq.end())){
				melt_1.push_front_target_native(NucCruc::GAP, false);
				melt_2.push_front_target_native(NucCruc::GAP, false);
			}
			else{	
				switch(iter.base()){
					case DNA3Seq::A:
						melt_1.push_front_target_native(NucCruc::A, false);
						melt_2.push_front_target_native(NucCruc::A, false);
						break;
					case DNA3Seq::T:
						melt_1.push_front_target_native(NucCruc::T, false);
						melt_2.push_front_target_native(NucCruc::T, false);
						break;
					case DNA3Seq::G:
						melt_1.push_front_target_native(NucCruc::G, false);
						melt_2.push_front_target_native(NucCruc::G, false);
						break;
					case DNA3Seq::C:
						melt_1.push_front_target_native(NucCruc::C, false);
						melt_2.push_front_target_native(NucCruc::C, false);
						break;
					default:
						// We have encountered a gap or unknown base
						melt_1.clear_target();
						melt_2.clear_target();
						break;
				};
				
				iter++;

				if(is_circular && (iter == seq.end()) ){
					iter = seq.begin();
				}
			}
			
			// Do we have enough bases to compare for primer 1
			if(melt_1.size_target() == primer_1_len){
			
				// Compute the melting temperatures
				// Tm for the plus strand
				float tm;

				// Have we found a valid amplicon?
				if(melt_1.anchor_reference(m_exact_match) && 
				  ( (tm = melt_1.Tm_reference()) >= m_tm)){
					
					// Primer 1 (-) upstream and Primer 1 (+) downstream
					
					// We have a match! Store everything in the SeqHit list
					const int primer_start = (int)bind_iter->start - (int)primer_1_len + 1;
					const int primer_stop = base_pos;
						
					const unsigned int match_start = (is_circular) ? wrap( primer_start, seq.size() ) :
						max(0, primer_start);
					const unsigned int match_stop = (is_circular) ?  wrap( primer_stop, seq.size() ) :
						min((int)(seq.size() - 1), primer_stop);
					
					// Only save this amplicon if it's length is greater (or equal to) the 
					// minimum length supplied by the user
					if( wrap( (int)(match_stop) - (int)(match_start) + 1, seq.size() ) 
						>= m_min_amplicon_len){
					
						// Save the location of the matching sequence
						SeqHit tmp(match_start, match_stop, 
							bind_iter->tm, '1', bind_iter->clamp3,
							tm, '1', melt_1.anchor_reference(),
							false,
							SeqHit::PRIMER_MATCH);
						
						tmp.primer_match_1(bind_iter->num_match);
						tmp.primer_match_2( melt_1.num_identity_reference() );
						
						tmp.primer_len_1(bind_iter->len);
						tmp.primer_len_2(primer_1_len);
						
						hit_list.push_back(tmp);

						list<GeneAnnotation>::iterator a_iter;
							
						for(a_iter = gene_list.begin();a_iter != gene_list.end();a_iter++){
								
							if(overlap(a_iter->start(), a_iter->stop(), match_start, match_stop, seq.size()) == true){
								// We found the product! Save the annotation that
								// matched.
								annot_iter_list.push_back(a_iter);
							}
						}
					}
				}

				melt_1.pop_back_target();
			}
			
			// Do we have enough bases to compare for primer 2
			if(melt_2.size_target() == primer_2_len){
				// Compute the melting temperatures
				// Tm for the plus strand
				float tm;

				// Have we found a valid amplicon?
				if(melt_2.anchor_reference(m_exact_match) && 
				  ( (tm = melt_2.Tm_reference()) >= m_tm)){
					// Primer 1 (-) upstream and Primer 2 (+) downstream
					
					// We have a match! Store everything in the SeqHit list
					// index() is 0 based and the NCBI ASN.1 entries are also 
					// 0 based.
					const int primer_start = (int)bind_iter->start - (int)primer_1_len + 1;
					const int primer_stop = base_pos;
						
					const unsigned int match_start = (is_circular) ? wrap(primer_start, seq.size()) :
						max(0, primer_start);
					const unsigned int match_stop = (is_circular) ? wrap(primer_stop, seq.size()) :
						min((int)(seq.size() - 1), primer_stop);
					
					// Only save this amplicon if it's length is greater (or equal to) the 
					// minimum length supplied by the user
					if( wrap( (int)(match_stop) - (int)(match_start) + 1, seq.size() ) 
						>= m_min_amplicon_len){

						// Save the location of the matching sequence
						SeqHit tmp(match_start, match_stop, 
							bind_iter->tm, '1', bind_iter->clamp3,
							tm, '2', melt_2.anchor_reference(),
							false,
							SeqHit::PRIMER_MATCH);
						
						tmp.primer_match_1(bind_iter->num_match);
						tmp.primer_match_2( melt_2.num_identity_reference() );
						
						tmp.primer_len_1(bind_iter->len);
						tmp.primer_len_2(primer_2_len);
						
						hit_list.push_back(tmp);

						list<GeneAnnotation>::iterator a_iter;
							
						for(a_iter = gene_list.begin();a_iter != gene_list.end();a_iter++){
								
							if(overlap(a_iter->start(), a_iter->stop(), match_start, match_stop, seq.size()) == true){
								// We found the product! Save the annotation that
								// matched.
								annot_iter_list.push_back(a_iter);
							}
						}
					}
				}

				melt_2.pop_back_target();
			}
		}
	}

	for(bind_iter = primer2_bind_minus.begin();bind_iter != primer2_bind_minus.end();bind_iter++){

		// Clear any existing sequence in the melting buffers
		melt_1.clear_target();
		melt_2.clear_target();

		// If this is a circular molecule, make len large enought to accomodate the
		// primer wrapping around.
		const int len = (is_circular) ? m_max_amplicon_len : 
			min((int)m_max_amplicon_len, (int)(seq.size() + pad_len) - (int)bind_iter->start);

		iter = (is_circular) ?  seq.begin() + wrap(bind_iter->start, seq.size()) :
				seq.begin() + max(0, (int)bind_iter->start);
		
		int base_pos = max(0, (int)bind_iter->start);
		
		for(int i = min(0, (int)bind_iter->start);i < len;i++, base_pos++){

			// Bind to the plus strand of the target DNA
			if((i < 0) || (iter == seq.end())){
				melt_1.push_front_target_native(NucCruc::GAP, false);
				melt_2.push_front_target_native(NucCruc::GAP, false);
			}
			else{
				switch(iter.base()){
					case DNA3Seq::A:
						melt_1.push_front_target_native(NucCruc::A, false);
						melt_2.push_front_target_native(NucCruc::A, false);
						break;
					case DNA3Seq::T:
						melt_1.push_front_target_native(NucCruc::T, false);
						melt_2.push_front_target_native(NucCruc::T, false);
						break;
					case DNA3Seq::G:
						melt_1.push_front_target_native(NucCruc::G, false);
						melt_2.push_front_target_native(NucCruc::G, false);
						break;
					case DNA3Seq::C:
						melt_1.push_front_target_native(NucCruc::C, false);
						melt_2.push_front_target_native(NucCruc::C, false);
						break;
					default:
						// We have encountered a gap or unknown base
						melt_1.clear_target();
						melt_2.clear_target();
						break;
				};
				
				iter++;

				if(is_circular && (iter == seq.end()) ){
					iter = seq.begin();
				}
			}
			
			// Do we have enough bases to compare for primer 1
			if(melt_1.size_target() == primer_1_len){
				// Compute the melting temperatures
				// Tm for the plus strand
				float tm;

				// Have we found a valid amplicon?
				if(melt_1.anchor_reference(m_exact_match) && 
				  ( (tm = melt_1.Tm_reference()) >= m_tm)){
					
					// Primer 2 (-) upstream and Primer 1 (+) downstream
					
					// We have a match! Store everything in the SeqHit list
					const int primer_start = (int)bind_iter->start - (int)primer_2_len + 1;
					const int primer_stop = base_pos;
					
					const unsigned int match_start = (is_circular) ? wrap(primer_start, seq.size()) :
						max(0, primer_start);
					const unsigned int match_stop = (is_circular) ? wrap(primer_stop, seq.size()) :
						min((int)(seq.size() - 1), primer_stop);
					
					// Only save this amplicon if it's length is greater (or equal to) the 
					// minimum length supplied by the user
					if( wrap( (int)(match_stop) - (int)(match_start) + 1, seq.size() ) 
						>= m_min_amplicon_len){

						// Save the location of the matching sequence
						SeqHit tmp(match_start, match_stop, 
							bind_iter->tm, '2', bind_iter->clamp3,
							tm, '1', melt_1.anchor_reference(),
							false,
							SeqHit::PRIMER_MATCH);
						
						tmp.primer_match_1(bind_iter->num_match);
						tmp.primer_match_2( melt_1.num_identity_reference() );
						
						tmp.primer_len_1(bind_iter->len);
						tmp.primer_len_2(primer_1_len);
						
						hit_list.push_back(tmp);

						list<GeneAnnotation>::iterator a_iter;
							
						for(a_iter = gene_list.begin();a_iter != gene_list.end();a_iter++){
								
							if(overlap(a_iter->start(), a_iter->stop(), match_start, match_stop, seq.size()) == true){
								// We found the product! Save the annotation that
								// matched.
								annot_iter_list.push_back(a_iter);
							}
						}
					}
				}

				melt_1.pop_back_target();
			}
			
			// Do we have enough bases to compare for primer 2
			if(melt_2.size_target() == primer_2_len){
			
				// Compute the melting temperatures
				// Tm for the plus strand
				float tm;

				// Have we found a valid amplicon?
				if(melt_2.anchor_reference(m_exact_match) && 
				  ( (tm = melt_2.Tm_reference()) >= m_tm)){
					// Primer 2 (-) upstream and Primer 2 (+) downstream
					
					// We have a match! Store everything in the SeqHit list
					const int primer_start = (int)bind_iter->start - (int)primer_2_len + 1;
					const int primer_stop = base_pos;
						
					const unsigned int match_start = (is_circular) ? wrap(primer_start, seq.size()) :
						max(0, primer_start);
					const unsigned int match_stop = (is_circular) ? wrap(primer_stop, seq.size()) :
						min((int)( seq.size() - 1), primer_stop);
					
					// Only save this amplicon if it's length is greater (or equal to) the 
					// minimum length supplied by the user
					if( wrap( (int)(match_stop) - (int)(match_start) + 1, seq.size() ) 
						>= m_min_amplicon_len){

						// Save the location of the matching sequence
						SeqHit tmp(match_start, match_stop, 
							bind_iter->tm, '2', bind_iter->clamp3,
							tm, '2', melt_1.anchor_reference(),
							false,
							SeqHit::PRIMER_MATCH);
						
						tmp.primer_match_1(bind_iter->num_match);
						tmp.primer_match_2( melt_1.num_identity_reference() );
						
						tmp.primer_len_1(bind_iter->len);
						tmp.primer_len_2(primer_2_len);
						
						hit_list.push_back(tmp);

						list<GeneAnnotation>::iterator a_iter;
							
						for(a_iter = gene_list.begin();a_iter != gene_list.end();a_iter++){
								
							if(overlap(a_iter->start(), a_iter->stop(), match_start, match_stop, seq.size()) == true){
								// We found the product! Save the annotation that
								// matched.
								annot_iter_list.push_back(a_iter);
							}
						}
					}
				}

				melt_2.pop_back_target();
			}
		}
	}
	
	// Use template specialization to define a comparison
	// operator for GeneAnnotation *. Other (non-WIN32) implementations
	// of the STL may use std::less instead of std::greater -- be carefull!
	annot_iter_list.sort(greater< list<GeneAnnotation>::iterator >());

	annot_iter_list.unique();
	
	return annot_iter_list;

}


// Search for binding and ligation sites for MOL-PCR.
//
// Here are all of the possible binding configurations for primers P1 and P2
// against a target sequence (for now, we assume that all sequences have both a 5'-3'
// and a 3'-5' strand -- obviously not true for single stranded viruses):
//
//                     3'-P1-5'3'-P2-5'
//	5'- =============================== -3'
//	3'- =============================== -5'
//         
//
//                     3'-P2-5'3'-P1-5'
//	5'- =============================== -3'
//	3'- =============================== -5'
//         
//                             
//	5'- =============================== -3'
//	3'- =============================== -5'
//         5'-P1-3'5'-P2-3'
//
//
//	5'- =============================== -3'
//	3'- =============================== -5'
//         5'-P2-3'5'-P1-3'
//
//                     3'-P1-5'3'-P1-5'
//	5'- =============================== -3'		* single primer amplification
//	3'- =============================== -5'
// 
//
//
//	5'- =============================== -3'		* single primer amplification
//	3'- =============================== -5'
//         5'-P2-3'5'-P2-3
//
std::list< std::list<GeneAnnotation>::iterator > DNAMol::find_MOL_primers(const DNA3Seq &m_primer_1, 
		const DNA3Seq &m_primer_2, const float &m_salt, const float &m_strand, 
		const float &m_tm, const unsigned int &m_exact_match, 
		const float &m_identity, std::list<SeqHit> &hit_list)
{

	list< list<GeneAnnotation>::iterator > annot_iter_list;
	
	// Check BOTH primer 1 and primer 2 against the minus
	// strand of the target sequence

	// Save the locations of all of the places that primer 1 and 2 bind to.
	list<PrimerInfo> bind;

	// Compute the Tm for the plus and minus strands at the same time!
	NucCruc melt_1;		// P1 binds to the minus strand?
	NucCruc melt_2;		// P2 binds to the minus strand?

	melt_1.Salt(m_salt);
	melt_2.Salt(m_salt);

	melt_1.Strand(m_strand);
	melt_2.Strand(m_strand);

	melt_1.IdentityCutoff(m_identity);
	melt_2.IdentityCutoff(m_identity);

	// Convert the DNA sequences of the primers into
	// strings for use in the Nucleic Crucible engine
	const string primer1 = m_primer_1.str();
	const string primer2 = m_primer_2.str();
	
	// Load the first primer seq as the reference
	melt_1.reference(primer1);
	melt_2.reference(primer2);

	DNA3Seq::iterator iter = seq.begin();
	
	const unsigned int primer_1_len = primer1.size();
	const unsigned int primer_2_len = primer2.size();

	// If this is a circular molecule, make len large enought to accomodate the
	// primer wrapping around.
	unsigned int len = (is_circular) ? seq.size() + max(primer_1_len, primer_2_len) - 1 : seq.size();
	
	// For non-periodic molecules, we need to allowed a primer to overhang the sequence.
	// Hence "Padding" sequences for appending and prepending to the target sequence
	const int pad_len = (is_circular) ? 0 : max(primer_1_len, primer_2_len);
	
	int i;

	for(i = -pad_len;i < ((int)len + pad_len);i++){

		if((i < 0) || (iter == seq.end())){
			// Add pre- and post-padding to the target sequence
			melt_1.push_back_target_native(NucCruc::GAP, false);
			melt_2.push_back_target_native(NucCruc::GAP, false);				
		}
		else{
			// Bind to the minus strand of the target DNA (use the switch statement
			// to pass the complement base -- saves a function call within push_back_target_native).
			switch(iter.base()){
				case DNA3Seq::A:
					melt_1.push_back_target_native(NucCruc::T, false);
					melt_2.push_back_target_native(NucCruc::T, false);
					break;
				case DNA3Seq::T:
					melt_1.push_back_target_native(NucCruc::A, false);
					melt_2.push_back_target_native(NucCruc::A, false);
					break;
				case DNA3Seq::G:
					melt_1.push_back_target_native(NucCruc::C, false);
					melt_2.push_back_target_native(NucCruc::C, false);
					break;
				case DNA3Seq::C:
					melt_1.push_back_target_native(NucCruc::G, false);
					melt_2.push_back_target_native(NucCruc::G, false);
					break;
				default:
					// We have encountered a gap or unknown base
					melt_1.clear_target();
					melt_2.clear_target();
					break;
			};
			
			iter++;
			
			if(is_circular && (iter == seq.end()) ){
				iter = seq.begin();
			}
		}
		
		// Do we have enough bases to compare for primer 1
		if(melt_1.size_target() == primer_1_len){
		
			// Compute the melting temperatures
			// Tm for the plus strand
			float tm;
			
			// Only consider primers that have m_exact_match bases exact match
			// at the 3' end
			if( (melt_1.anchor3_reference() >= m_exact_match) && 
			  ( ( tm = melt_1.Tm_reference()) >= m_tm) ){

				// Store the location and Tm for this primer
				PrimerInfo tmp;

				tmp.id = PrimerInfo::PRIMER_1;
				tmp.tm = tm;
				tmp.clamp3 = melt_1.anchor3_reference();
				tmp.start = i + 1;
				tmp.num_match = melt_1.num_identity_reference();
				tmp.len = primer_1_len;
				
				bind.push_back(tmp);
			}

			melt_1.pop_front_target();
		}
		
		// Do we have enough bases to compare for primer 2
		if(melt_2.size_target() == primer_2_len){
			// Compute the melting temperatures
			// Tm for the plus strand
			float tm;
			
			// Only consider primers that have m_exact_match bases exact match
			// at the 3' end
			if( (melt_2.anchor3_reference() >= m_exact_match) && 
			  ( ( tm = melt_2.Tm_reference() ) >= m_tm)){
				
				// Store the location and Tm for this primer
				PrimerInfo tmp;

				tmp.id = PrimerInfo::PRIMER_2;
				tmp.tm = tm;
				tmp.clamp3 = melt_2.anchor3_reference();
				tmp.start = i + 1;
				tmp.num_match = melt_2.num_identity_reference();
				tmp.len = primer_2_len;
				
				bind.push_back(tmp);
			}

			melt_2.pop_front_target();
		}
	}	
	
	// Test the possible primer arrangments that can produce an amplicon:
	// 
	list<PrimerInfo>::const_iterator bind_iter;

	for(bind_iter = bind.begin();bind_iter != bind.end();bind_iter++){

		// Clear any existing sequence in the melting buffers
		melt_1.clear_target();
		melt_2.clear_target();

		const unsigned int index_stop = (is_circular) ?  bind_iter->start + max(primer_1_len, primer_2_len) :
			min( seq.size(), bind_iter->start + max(primer_1_len, primer_2_len) );

		iter = seq.begin() + wrap( bind_iter->start, seq.size() );
		
		int base_pos = bind_iter->start;

		for(unsigned int j = bind_iter->start;j < index_stop;j++, base_pos++){
			
			// Bind to the minus strand of the target DNA (use the switch statement
			// to pass the complement base -- saves a function call within push_back_target_native).
			switch( iter.base() ){
			case DNA3Seq::A:
				melt_1.push_back_target_native(NucCruc::T, false);
				melt_2.push_back_target_native(NucCruc::T, false);
				break;
			case DNA3Seq::T:
				melt_1.push_back_target_native(NucCruc::A, false);
				melt_2.push_back_target_native(NucCruc::A, false);
				break;
			case DNA3Seq::G:
				melt_1.push_back_target_native(NucCruc::C, false);
				melt_2.push_back_target_native(NucCruc::C, false);
				break;
			case DNA3Seq::C:
				melt_1.push_back_target_native(NucCruc::G, false);
				melt_2.push_back_target_native(NucCruc::G, false);
				break;
			default:
				// We have encountered a gap or unknown base
				melt_1.clear_target();
				melt_2.clear_target();
				break;
			};
			
			iter++;
			
			if(is_circular && (iter == seq.end()) ){
				iter = seq.begin();
			}
			
			// Do we have enough bases to compare for primer 1
			if(melt_1.size_target() == primer_1_len){
			
				// Compute the melting temperatures
				// Tm for the plus strand
				float tm;

				// Have we found a valid amplicon? Downstream amplicons must have m_exact_match
				// bases of exact match at the 5' end
				if( (melt_1.anchor5_reference() >= m_exact_match) && 
				  ( (tm = melt_1.Tm_reference()) >= m_tm)){
					
					// Primer x upstream and Primer 1 downstream
					
					// We have a match! Store everything in the SeqHit list
					const int primer_start = (int)bind_iter->start - 
						(int)( (bind_iter->id == PrimerInfo::PRIMER_1) ? primer_1_len 
						: primer_2_len);
					const int primer_stop = base_pos;
						
					const unsigned int match_start = (is_circular) ? wrap( primer_start, seq.size() ) :
						max(0, primer_start);
					const unsigned int match_stop = (is_circular) ?  wrap( primer_stop, seq.size() ) :
						min((int)(seq.size() - 1), primer_stop);
					
					// Save the location of the matching sequence
					SeqHit tmp(match_start, match_stop, 
						bind_iter->tm, 
						(bind_iter->id == PrimerInfo::PRIMER_1) ? '1' : '2', 
						bind_iter->clamp3,
						tm, '1', melt_1.anchor5_reference(),
						false,
						SeqHit::MOL_PRIMER_MATCH);
					
					tmp.primer_match_1(bind_iter->num_match);
					tmp.primer_match_2( melt_1.num_identity_reference() );

					tmp.primer_len_1(bind_iter->len);
					tmp.primer_len_2(primer_1_len);

					hit_list.push_back(tmp);
					
					list<GeneAnnotation>::iterator a_iter;
						
					for(a_iter = gene_list.begin();a_iter != gene_list.end();a_iter++){
							
						if(overlap(a_iter->start(), a_iter->stop(), match_start, match_stop, seq.size()) == true){
							// We found the product! Save the annotation that
							// matched.
							annot_iter_list.push_back(a_iter);
						}
					}
				}

				// Don't pop bases from m_melt -- this allows us to check different length primers in
				// the same loop
				// melt_1.pop_back_target();
			}
			
			// Do we have enough bases to compare for primer 2
			if(melt_2.size_target() == primer_2_len){
				// Compute the melting temperatures
				// Tm for the plus strand
				float tm;

				// Have we found a valid amplicon? Downstream amplicons must have m_exact_match
				// bases of exact match at the 5' end
				if( (melt_2.anchor5_reference() >= m_exact_match) && 
				  ( (tm = melt_2.Tm_reference()) >= m_tm)){
					// Primer x upstream and Primer 2 downstream
					
					// We have a match! Store everything in the SeqHit list
					// index() is 0 based and the NCBI ASN.1 entries are also 
					// 0 based.
					const int primer_start = (int)bind_iter->start - 
						(int)( (bind_iter->id == PrimerInfo::PRIMER_1) ? primer_1_len 
						: primer_2_len);

					const int primer_stop = base_pos;
						
					const unsigned int match_start = (is_circular) ? wrap(primer_start, seq.size()) :
						max(0, primer_start);
					const unsigned int match_stop = (is_circular) ? wrap(primer_stop, seq.size()) :
						min((int)(seq.size() - 1), primer_stop);

					// Save the location of the matching sequence
					SeqHit tmp(match_start, match_stop, 
						bind_iter->tm, 
						(bind_iter->id == PrimerInfo::PRIMER_1) ? '1' : '2', 
						bind_iter->clamp3,
						tm, '2', melt_2.anchor5_reference(),
						false,
						SeqHit::MOL_PRIMER_MATCH);
					
					tmp.primer_match_1(bind_iter->num_match);
					tmp.primer_match_2( melt_2.num_identity_reference() );

					tmp.primer_len_1(bind_iter->len);
					tmp.primer_len_2(primer_2_len);
					
					hit_list.push_back(tmp);

					list<GeneAnnotation>::iterator a_iter;
							
					for(a_iter = gene_list.begin();a_iter != gene_list.end();a_iter++){
								
						if(overlap(a_iter->start(), a_iter->stop(), match_start, match_stop, seq.size()) == true){
							// We found the product! Save the annotation that
							// matched.
							annot_iter_list.push_back(a_iter);
						}
					}
				}
				
				// Don't pop bases from m_melt -- this allows us to check different length primers in
				// the same loop
				// melt_2.pop_back_target();
			}
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	// Clear the results for the minus strand
	iter = seq.begin();
	bind.clear();
	melt_1.clear_target();
	melt_2.clear_target();

	// Check the MOLigos that bind to the plus DNA strand
	for(i = -pad_len;i < ((int)len + pad_len);i++){

		if((i < 0) || (iter == seq.end())){
			// Add pre- and post-padding to the target sequence
			melt_1.push_front_target_native(NucCruc::GAP, false);
			melt_2.push_front_target_native(NucCruc::GAP, false);				
		}
		else{
			// Bind to the plus strand of the target DNA
			switch( iter.base() ){
				case DNA3Seq::A:
					melt_1.push_front_target_native(NucCruc::A, false);
					melt_2.push_front_target_native(NucCruc::A, false);
					break;
				case DNA3Seq::T:
					melt_1.push_front_target_native(NucCruc::T, false);
					melt_2.push_front_target_native(NucCruc::T, false);
					break;
				case DNA3Seq::G:
					melt_1.push_front_target_native(NucCruc::G, false);
					melt_2.push_front_target_native(NucCruc::G, false);
					break;
				case DNA3Seq::C:
					melt_1.push_front_target_native(NucCruc::C, false);
					melt_2.push_front_target_native(NucCruc::C, false);
					break;
				default:
					// We have encountered a gap or unknown base
					melt_1.clear_target();
					melt_2.clear_target();
					break;
			};
			
			iter++;
			
			if(is_circular && (iter == seq.end()) ){
				iter = seq.begin();
			}
		}
		
		// Do we have enough bases to compare for primer 1
		if(melt_1.size_target() == primer_1_len){
		
			// Compute the melting temperatures
			// Tm for the plus strand
			float tm;
			
			// Only consider primers that have m_exact_match bases exact match
			// at the 5' end
			if( (melt_1.anchor5_reference() >= m_exact_match) && 
			  ( ( tm = melt_1.Tm_reference()) >= m_tm) ){

				// Store the location and Tm for this primer
				PrimerInfo tmp;

				tmp.id = PrimerInfo::PRIMER_1;
				tmp.tm = tm;
				tmp.clamp3 = melt_1.anchor5_reference();
				tmp.start = i + 1;
				tmp.num_match = melt_1.num_identity_reference();
				tmp.len = primer_1_len;
				
				bind.push_back(tmp);
			}

			melt_1.pop_back_target();
		}
		
		// Do we have enough bases to compare for primer 2
		if(melt_2.size_target() == primer_2_len){
		
			// Compute the melting temperatures
			// Tm for the plus strand
			float tm;
			
			// Only consider primers that have m_exact_match bases exact match
			// at the 5' end
			if( (melt_2.anchor5_reference() >= m_exact_match) && 
			  ( ( tm = melt_2.Tm_reference() ) >= m_tm)){
				
				// Store the location and Tm for this primer
				PrimerInfo tmp;

				tmp.id = PrimerInfo::PRIMER_2;
				tmp.tm = tm;
				tmp.clamp3 = melt_2.anchor5_reference();
				tmp.start = i + 1;
				tmp.num_match = melt_2.num_identity_reference();
				tmp.len = primer_2_len;
				
				bind.push_back(tmp);
			}

			melt_2.pop_back_target();
		}
	}	
	
	// Test the possible primer arrangments that can result in ligation:
	for(bind_iter = bind.begin();bind_iter != bind.end();bind_iter++){

		// Clear any existing sequence in the melting buffers
		melt_1.clear_target();
		melt_2.clear_target();

		const unsigned int index_stop = (is_circular) ?  bind_iter->start + max(primer_1_len, primer_2_len) :
			min( seq.size(), bind_iter->start + max(primer_1_len, primer_2_len) );

		iter = seq.begin() + wrap( bind_iter->start, seq.size() );
		
		int base_pos = bind_iter->start;

		for(unsigned int j = bind_iter->start;j < index_stop;j++, base_pos++){
			
			switch(iter.base()){
			case DNA3Seq::A:
				melt_1.push_front_target_native(NucCruc::A, false);
				melt_2.push_front_target_native(NucCruc::A, false);
				break;
			case DNA3Seq::T:
				melt_1.push_front_target_native(NucCruc::T, false);
				melt_2.push_front_target_native(NucCruc::T, false);
				break;
			case DNA3Seq::G:
				melt_1.push_front_target_native(NucCruc::G, false);
				melt_2.push_front_target_native(NucCruc::G, false);
				break;
			case DNA3Seq::C:
				melt_1.push_front_target_native(NucCruc::C, false);
				melt_2.push_front_target_native(NucCruc::C, false);
				break;
			default:
				// We have encountered a gap or unknown base
				melt_1.clear_target();
				melt_2.clear_target();
				break;
			};
			
			iter++;
			
			if(is_circular && (iter == seq.end()) ){
				iter = seq.begin();
			}
			
			// Do we have enough bases to compare for primer 1
			if(melt_1.size_target() == primer_1_len){
			
				// Compute the melting temperatures
				// Tm for the plus strand
				float tm;

				// Have we found a valid amplicon? Downstream amplicons must have m_exact_match
				// bases of exact match at the 3' end
				if( (melt_1.anchor3_reference() >= m_exact_match) && 
				  ( (tm = melt_1.Tm_reference()) >= m_tm)){
					
					// Primer x upstream and Primer 1 downstream
					
					// We have a match! Store everything in the SeqHit list
					const int primer_start = (int)bind_iter->start - 
						(int)( (bind_iter->id == PrimerInfo::PRIMER_1) ? primer_1_len 
						: primer_2_len);

					const int primer_stop = base_pos;
						
					const unsigned int match_start = (is_circular) ? wrap( primer_start, seq.size() ) :
						max(0, primer_start);
					const unsigned int match_stop = (is_circular) ?  wrap( primer_stop, seq.size() ) :
						min((int)(seq.size() - 1), primer_stop);
					
					// Save the location of the matching sequence
					SeqHit tmp(match_start, match_stop, 
						bind_iter->tm, 
						(bind_iter->id == PrimerInfo::PRIMER_1) ? '1' : '2', 
						bind_iter->clamp3,
						tm, '1', melt_1.anchor3_reference(),
						true,
						SeqHit::MOL_PRIMER_MATCH);
					
					tmp.primer_match_1(bind_iter->num_match);
					tmp.primer_match_2( melt_1.num_identity_reference() );

					tmp.primer_len_1(bind_iter->len);
					tmp.primer_len_2(primer_1_len);
					
					hit_list.push_back(tmp);
						
					list<GeneAnnotation>::iterator a_iter;
						
					for(a_iter = gene_list.begin();a_iter != gene_list.end();a_iter++){
							
						if(overlap(a_iter->start(), a_iter->stop(), match_start, match_stop, seq.size()) == true){
							// We found the product! Save the annotation that
							// matched.
							annot_iter_list.push_back(a_iter);
						}
					}
				}

				// Don't pop bases from m_melt -- this allows us to check different length primers in
				// the same loop
				// melt_1.pop_back_target();
			}
			
			// Do we have enough bases to compare for primer 2
			if(melt_2.size_target() == primer_2_len){
			
				// Compute the melting temperatures
				// Tm for the plus strand
				float tm;

				// Have we found a valid amplicon? Downstream amplicons must have m_exact_match
				// bases of exact match at the 3' end
				if( (melt_2.anchor3_reference() >= m_exact_match) && 
				  ( (tm = melt_2.Tm_reference()) >= m_tm)){
					// Primer x upstream and Primer 2 downstream
					
					// We have a match! Store everything in the SeqHit list
					// index() is 0 based and the NCBI ASN.1 entries are also 
					// 0 based.
					const int primer_start = (int)bind_iter->start - 
						(int)( (bind_iter->id == PrimerInfo::PRIMER_1) ? primer_1_len 
						: primer_2_len);

					const int primer_stop = base_pos;
						
					const unsigned int match_start = (is_circular) ? wrap(primer_start, seq.size()) :
						max(0, primer_start);
					const unsigned int match_stop = (is_circular) ? wrap(primer_stop, seq.size()) :
						min((int)(seq.size() - 1), primer_stop);

					// Save the location of the matching sequence
					SeqHit tmp(match_start, match_stop, 
						bind_iter->tm, 
						(bind_iter->id == PrimerInfo::PRIMER_1) ? '1' : '2', 
						bind_iter->clamp3,
						tm, '2', melt_2.anchor3_reference(),
						true,
						SeqHit::MOL_PRIMER_MATCH);
					
					tmp.primer_match_1(bind_iter->num_match);
					tmp.primer_match_2( melt_2.num_identity_reference() );

					tmp.primer_len_1(bind_iter->len);
					tmp.primer_len_2(primer_2_len);
					
					hit_list.push_back(tmp);

					list<GeneAnnotation>::iterator a_iter;
							
					for(a_iter = gene_list.begin();a_iter != gene_list.end();a_iter++){
								
						if(overlap(a_iter->start(), a_iter->stop(), match_start, match_stop, seq.size()) == true){
							// We found the product! Save the annotation that
							// matched.
							annot_iter_list.push_back(a_iter);
						}
					}
				}
				
				// Don't pop bases from m_melt -- this allows us to check different length primers in
				// the same loop
				// melt_2.pop_back_target();
			}
		}
	}

	// Use template specialization to define a comparison
	// operator for GeneAnnotation *. Other (non-WIN32) implementations
	// of the STL may use std::less instead of std::greater -- be carefull!
	annot_iter_list.sort(greater< list<GeneAnnotation>::iterator >());

	annot_iter_list.unique();
	
	return annot_iter_list;

}


list< list<GeneAnnotation>::iterator > DNAMol::find_probe(const DNA3Seq &m_probe, 
		const float &m_salt, const float &m_strand, const float &m_tm,
		const float &m_identity, std::list<SeqHit> &hit_list)
{
	list< list<GeneAnnotation>::iterator > annot_iter_list;
	
	// Compute the Tm for the plus and minus strands at the same time!
	NucCruc melt_plus;		// Bind to the plus strand?
	NucCruc melt_minus;		// Bind to the minus strand?

	melt_plus.Salt(m_salt);
	melt_plus.Strand(m_strand);

	melt_minus.Salt(m_salt);
	melt_minus.Strand(m_strand);

	melt_plus.IdentityCutoff(m_identity);
	melt_minus.IdentityCutoff(m_identity);

	// Convert the DNA sequence into a string for use in the Nucleic Crucible engine
	const string probe = m_probe.str();
	
	// Load the probe seq as the reference
	melt_plus.reference(probe);
	melt_minus.reference(probe);

	DNA3Seq::iterator iter = seq.begin();
	
	const unsigned int probe_len = probe.size();

	// If this is a circular molecule, make len large enought to accomodate the
	// probe wrapping around.
	const unsigned int len = (is_circular) ? seq.size() + probe_len - 1 : seq.size();
	
	// "Padding" sequences for appending and prepending to the target sequence
	const int pad_len = (is_circular) ? 0 : probe_len;
	
	for(int i =  -pad_len;i < (int)len + pad_len;i++){
		
		if((i < 0) || (iter == seq.end())){
			// Add pre- and post-padding to the target sequence
			melt_minus.push_back_target_native(NucCruc::GAP, false);
			melt_plus.push_front_target_native(NucCruc::GAP, false);				
		}
		else{
			
			switch(iter.base()){
				case DNA3Seq::A:
					melt_minus.push_back_target_native(NucCruc::A, true);
					melt_plus.push_front_target_native(NucCruc::A, false);
					break;
				case DNA3Seq::T:
					melt_minus.push_back_target_native(NucCruc::T, true);
					melt_plus.push_front_target_native(NucCruc::T, false);
					break;
				case DNA3Seq::G:
					melt_minus.push_back_target_native(NucCruc::G, true);
					melt_plus.push_front_target_native(NucCruc::G, false);
					break;
				case DNA3Seq::C:
					melt_minus.push_back_target_native(NucCruc::C, true);
					melt_plus.push_front_target_native(NucCruc::C, false);
					break;
				default:
					// We have encountered a gap or unknown base
					melt_minus.clear_target();
					melt_plus.clear_target();
			};
			
			iter++;

			if(is_circular && (iter == seq.end()) ){
				iter = seq.begin();
			}
		}
		
		// Do we have enough bases to compare
		if(melt_plus.size_target() == probe_len){
		
			// Compute the melting temperatures
			// Tm for the plus strand
			float tm = melt_plus.Tm_reference();

			if(tm >= m_tm){

				// index() is 0 based and the NCBI ASN.1 entries are also 
				// 0 based.
				const int probe_start = i  + 1 - probe_len;
				const int probe_stop = i;
				
				const unsigned int match_start = (is_circular) ? wrap(probe_start, seq.size()) :
					max(0, probe_start);
				const unsigned int match_stop = (is_circular) ? wrap(probe_stop, seq.size()) :
					min((int)(seq.size() - 1), probe_stop);
				
				// Save the location of the matching sequence
				SeqHit tmp(match_start, match_stop, tm, true, SeqHit::PROBE_MATCH);
				
				tmp.probe_match( melt_plus.num_identity_reference() );
				tmp.probe_len(probe_len);
				
				hit_list.push_back(tmp);

				list<GeneAnnotation>::iterator a_iter;
				
				for(a_iter = gene_list.begin();a_iter != gene_list.end();a_iter++){
					
					if(overlap(a_iter->start(), a_iter->stop(), match_start, match_stop, seq.size()) == true){
						// We found the product! Save the annotation that
						// matched.
						annot_iter_list.push_back(a_iter);
					}
				}
			}

			// Tm for the minus strand
			tm = melt_minus.Tm_reference();

			if(tm >= m_tm){

				// index() is 0 based and the NCBI ASN.1 entries are also 
				// 0 based.
				const int probe_start = i  + 1 - probe_len;
				const int probe_stop = i;
				
				const unsigned int match_start = (is_circular) ? wrap(probe_start, seq.size()) :
					max(0, probe_start);
				const unsigned int match_stop = (is_circular) ? wrap(probe_stop, seq.size()) :
					min((int)(seq.size() - 1), probe_stop);
				
				// Save the location of the matching sequence
				SeqHit tmp(match_start, match_stop, tm, false, SeqHit::PROBE_MATCH);
				
				tmp.probe_match( melt_minus.num_identity_reference() );
				tmp.probe_len(probe_len);

				hit_list.push_back(tmp);

				list<GeneAnnotation>::iterator a_iter;
				
				for(a_iter = gene_list.begin();a_iter != gene_list.end();a_iter++){
					
					if(overlap(a_iter->start(), a_iter->stop(), match_start, match_stop, seq.size()) == true){
						// We found the product! Save the annotation that
						// matched.
						annot_iter_list.push_back(a_iter);
					}
				}
			}

			melt_minus.pop_front_target();
			melt_plus.pop_back_target();
		}		
	}	

	// Use template specialization to define a comparison
	// operator for GeneAnnotation *. Other (non-WIN32) implementations
	// of the STL may use std::less instead of std::greater -- be carefull!
	annot_iter_list.sort(greater< list<GeneAnnotation>::iterator >());

	annot_iter_list.unique();
	
	return annot_iter_list;
}

string primer_test(const DNA3Seq &m_primer_1, 
		const DNA3Seq &m_primer_2, const float &m_salt, const float &m_strand, 
		const float &m_tm, const bool &m_display_seq /* = false */)
{
	stringstream sout;

	
	NucCruc melt;
	float tm;

	melt.Salt(m_salt);
	melt.Strand(m_strand);
	
	sout << "****** Primer info ****** " << ENDL;

	if(m_display_seq){
	
		sout << "Primer 1 = " << m_primer_1 << ENDL;
		sout << "Primer 2 = " << m_primer_2 << ENDL;
	}

	sout << "Primer 1 %G+C = " << 100.0f*melt.gc_content(m_primer_1.str()) << ENDL;
	sout << "Primer 2 %G+C = " << 100.0f*melt.gc_content(m_primer_2.str()) << ENDL;

	// The hetero-dimer calculation requires manual addition of the dangling end bases, i.e. "-"

	tm = melt.MaxDimerTm("-" + m_primer_1.str() + "-", "-" + m_primer_2.str() + "-", true);

	sout << "Primer 1 / Primer 2 dimer Tm = " 
		<< max(tm, 0.0f) << ENDL;

	tm = melt.MaxHairPinTm(m_primer_1.str());

	sout << "Primer 1 hairpin Tm = " << max(tm, 0.0f) << ENDL;

	tm = melt.MaxHairPinTm(m_primer_2.str());

	sout << "Primer 2 hairpin Tm = " << max(tm, 0.0f) << ENDL;
	
	sout << "-----------------------------------------------------------------------" 
		 << "-----------------------------------------------------------------------" << ENDL;

	return sout.str();
}


