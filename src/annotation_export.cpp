// This program was prepared by the Regents of the University of California at 
// Los Alamos National Laboratory (the University) under contract No. W-7405-ENG-36 
// with the U.S. Department of Energy (DOE). All rights in the program are reserved 
// by the DOE and the University.  Permission is granted to the public to copy and 
// use this software without charge, provided that this Notice and any statement of 
// authorship are reproduced on all copies.  Neither the U.S. Government nor the 
// University makes any warranty, express or implied, or assumes any liability or 
// responsibility for the use of this software.

#include "stdafx.h"

#include "annotation.h"
#include <fstream>

using namespace std;

// how many bases are on a line?
#define	FASTA_LINE_LENGTH	70
#define	GBK_LINE_LENGTH	58
#define	GBK_LINE_PREFIX	21

void format_gbk_record(ofstream &m_fout, const string &m_name, const string &m_value, 
	const bool &m_quote_value, const bool &m_no_word_breaks = true);

void DNAMol::export_fasta_genome(ofstream &m_fout)
{
	// Write the entire genome as a fasta file
	m_fout << '>';
	
	if(sip){
		char buffer[256];
		SeqIdPtr tmp_id = sip;
		
		while(tmp_id){
			SeqIdPrint(tmp_id, buffer, '|');
			m_fout << buffer;
			
			tmp_id = tmp_id->next;
			
			m_fout << '|';
		}
		
		m_fout << ' ';
	}
	
	if(info(DNAMol::TAXA_NAME) != ""){
		m_fout << info(DNAMol::TAXA_NAME) << " ";
	}
	else{
		if(info(DNAMol::GENUS) != ""){
			m_fout << info(DNAMol::GENUS) << " ";
		}
		
		if(info(DNAMol::SPECIES) != ""){
			m_fout << info(DNAMol::SPECIES) << " ";
		}
		
		if(info(DNAMol::SUBSPECIES) != ""){
			m_fout << info(DNAMol::SUBSPECIES) << " ";
		}
	}
	
	if(info(DNAMol::SOURCE) != ""){
		m_fout << "[" << info(DNAMol::SOURCE) << "] ";
	}
	
	m_fout << endl;
	
	DNA3Seq::iterator iter;
	unsigned int count = 1;
	
	for(iter = seq.begin();iter != seq.end();iter++){
		m_fout << iter.c_base();
		
		if(count == FASTA_LINE_LENGTH){
			m_fout << endl;
			count = 1;
		}
		else{
			count ++;
		}
	}
	
	if(count != 0){
		m_fout << endl;
	}
	
	m_fout << endl;
}

void DNAMol::export_fasta_dna(ofstream &m_fout, const list<unsigned int> &m_annot)
{
	// Create a fast lookup table for annotations to export
	vector<bool> annot_to_export(GeneAnnotation::LAST_ANNOT);
	
	list<unsigned int>::const_iterator annot_iter;
	
	for(annot_iter = m_annot.begin();annot_iter != m_annot.end();annot_iter++){
	
		if(*annot_iter >= GeneAnnotation::LAST_ANNOT){
			throw "Export annotation out of bounds!";
		}
		
		annot_to_export[*annot_iter] = true;
	}
	
	list<GeneAnnotation>::const_iterator iter;
	
	for(iter = gene_list.begin();iter != gene_list.end();iter++){
		if(annot_to_export[iter->type()] == false){
			continue;
		}
		
		// Note that subseq automatically returns the DNA sequence in
		// the correct frame (and handles segmented genes too)
		DNA3Seq sub_seq = iter->subseq( seq, iter->start(), iter->stop() );
		
		m_fout << '>' << iter->seq_id_str() << " " ;
		
		switch( iter->type() ){
			case GeneAnnotation::CDS:
				m_fout << "CDS ";
				break;
			case GeneAnnotation::GENE:
				m_fout << "GENE ";
				break;
			case GeneAnnotation::PSEUDO_GENE:
				m_fout << "PSEUDO-GENE ";
				break;
			case GeneAnnotation::RNA:
				m_fout << "RNA ";
				break;
			case GeneAnnotation::tRNA:
				m_fout << "tRNA ";
				break;
			case GeneAnnotation::NONE:
				m_fout << "Intergenic space ";
				break;
			case GeneAnnotation::PRIMER:
				m_fout << "PCR Primer ";
				break;
			case GeneAnnotation::TFBS:
				m_fout << "Transcription factor binding site ";
				break;
			case GeneAnnotation::USER:
				m_fout << "User defined ";
				break;
			default:
				m_fout << "Misc ";
				break;
		};

		if(iter->info(GeneAnnotation::LOCUS) != ""){
			m_fout << iter->info(GeneAnnotation::LOCUS) << " ";
		}
		else{
			if(iter->info(GeneAnnotation::LOCUS_TAG) != ""){
				m_fout << iter->info(GeneAnnotation::LOCUS_TAG) << " ";
			}
		}
		
		if(iter->info(GeneAnnotation::PRODUCT) != ""){
			m_fout << iter->info(GeneAnnotation::PRODUCT);
		}

		if(iter->info(GeneAnnotation::EC) != ""){
			m_fout << iter->info(GeneAnnotation::EC);
		}

		m_fout << endl;
		
		DNA3Seq::iterator iter;
		unsigned int count = 1;

		for(iter = sub_seq.begin();iter != sub_seq.end();iter++){
			m_fout << iter.c_base();

			if(count == FASTA_LINE_LENGTH){
				m_fout << endl;
				count = 1;
			}
			else{
				count ++;
			}
		}

		if(count != 0){
			m_fout << endl;
		}

		m_fout << endl;
	}
}

void DNAMol::export_fasta_dna(ofstream &m_fout, const vector<int> &m_gi)
{
	list<GeneAnnotation>::const_iterator iter;
	
	for(iter = gene_list.begin();iter != gene_list.end();iter++){
	
		// Is this gi in the list?
		const int gi = iter->gi();
		vector<int>::const_iterator gi_iter = lower_bound(m_gi.begin(), m_gi.end(), gi);
		
		if( !( ( gi_iter != m_gi.end() ) && (gi == *gi_iter) ) ){
			continue;
		}
		
		// Note that subseq automatically returns the DNA sequence in
		// the correct frame (and handles segmented genes too)
		DNA3Seq sub_seq = iter->subseq( seq, iter->start(), iter->stop() );
		
		m_fout << '>' << iter->seq_id_str() << " " ;
		
		switch( iter->type() ){
			case GeneAnnotation::CDS:
				m_fout << "CDS ";
				break;
			case GeneAnnotation::GENE:
				m_fout << "GENE ";
				break;
			case GeneAnnotation::PSEUDO_GENE:
				m_fout << "PSEUDO-GENE ";
				break;
			case GeneAnnotation::RNA:
				m_fout << "RNA ";
				break;
			case GeneAnnotation::tRNA:
				m_fout << "tRNA ";
				break;
			case GeneAnnotation::NONE:
				m_fout << "Intergenic space ";
				break;
			case GeneAnnotation::PRIMER:
				m_fout << "PCR Primer ";
				break;
			case GeneAnnotation::TFBS:
				m_fout << "Transcription factor binding site ";
				break;
			case GeneAnnotation::USER:
				m_fout << "User defined ";
				break;
			default:
				m_fout << "Misc ";
				break;
		};

		if(iter->info(GeneAnnotation::LOCUS) != ""){
			m_fout << iter->info(GeneAnnotation::LOCUS) << " ";
		}
		else{
			if(iter->info(GeneAnnotation::LOCUS_TAG) != ""){
				m_fout << iter->info(GeneAnnotation::LOCUS_TAG) << " ";
			}
		}
		
		if(iter->info(GeneAnnotation::PRODUCT) != ""){
			m_fout << iter->info(GeneAnnotation::PRODUCT);
		}

		if(iter->info(GeneAnnotation::EC) != ""){
			m_fout << iter->info(GeneAnnotation::EC);
		}

		m_fout << endl;
		
		DNA3Seq::iterator iter;
		unsigned int count = 1;

		for(iter = sub_seq.begin();iter != sub_seq.end();iter++){
			m_fout << iter.c_base();

			if(count == FASTA_LINE_LENGTH){
				m_fout << endl;
				count = 1;
			}
			else{
				count ++;
			}
		}

		if(count != 0){
			m_fout << endl;
		}

		m_fout << endl;
	}
}

void DNAMol::export_fasta_protein(ofstream &m_fout, const list<unsigned int> &m_annot)
{
	// Create a fast lookup table for annotations to export
	vector<bool> annot_to_export(GeneAnnotation::LAST_ANNOT);
	
	list<unsigned int>::const_iterator annot_iter;
	
	for(annot_iter = m_annot.begin();annot_iter != m_annot.end();annot_iter++){
	
		if(*annot_iter >= GeneAnnotation::LAST_ANNOT){
			throw "Export annotation out of bounds!";
		}
		
		annot_to_export[*annot_iter] = true;
	}
	
	list<GeneAnnotation>::const_iterator iter;
	
	for(iter = gene_list.begin();iter != gene_list.end();iter++){
		
		if(annot_to_export[iter->type()] == false){
			continue;
		}
		
		// Skip genes that do not also have CDS records
		if( (iter->type() == GeneAnnotation::GENE) && !iter->gene_has_cds() ){
			continue;
		}
		
		m_fout << '>' << iter->seq_id_str() << " " ;
		
		bool trim_stop_codon = true;
		
		switch( iter->type() ){
			case GeneAnnotation::CDS:
				m_fout << "CDS ";
				trim_stop_codon = true;
				break;
			case GeneAnnotation::GENE:
				m_fout << "GENE ";
				trim_stop_codon = true;
				break;
			case GeneAnnotation::PSEUDO_GENE:
				m_fout << "PSEUDO-GENE ";
				trim_stop_codon = false;
				break;
			case GeneAnnotation::RNA:
				m_fout << "RNA ";
				trim_stop_codon = false;
				break;
			case GeneAnnotation::tRNA:
				m_fout << "tRNA ";
				trim_stop_codon = false;
				break;
			case GeneAnnotation::NONE:
				m_fout << "Intergenic space ";
				trim_stop_codon = false;
				break;
			case GeneAnnotation::PRIMER:
				m_fout << "PCR Primer ";
				trim_stop_codon = false;
				break;
			case GeneAnnotation::TFBS:
				m_fout << "Transcription factor binding site ";
				trim_stop_codon = false;
				break;
			case GeneAnnotation::USER:
				m_fout << "User defined ";
				trim_stop_codon = false;
				break;
			default:
				m_fout << "Misc ";
				trim_stop_codon = false;
				break;
		};

		if(iter->info(GeneAnnotation::LOCUS) != ""){
			m_fout << iter->info(GeneAnnotation::LOCUS) << " ";
		}
		else{
			if(iter->info(GeneAnnotation::LOCUS_TAG) != ""){
				m_fout << iter->info(GeneAnnotation::LOCUS_TAG) << " ";
			}
		}
		
		if(iter->info(GeneAnnotation::PRODUCT) != ""){
			m_fout << iter->info(GeneAnnotation::PRODUCT);
		}

		if(iter->info(GeneAnnotation::EC) != ""){
			m_fout << iter->info(GeneAnnotation::EC);
		}

		m_fout << endl;
		
		// Note that subseq automatically returns the DNA sequence in
		// the correct frame (and handles segmented genes too)
		DNA3Seq sub_seq = iter->subseq( seq, iter->cds_start(), iter->cds_stop() );
		
		const string protein_seq = dna_to_protein(sub_seq, 0, trim_stop_codon);
		
		string::const_iterator iter;
		unsigned int count = 1;

		for(iter = protein_seq.begin();iter != protein_seq.end();iter++){
			m_fout << *iter;

			if(count == FASTA_LINE_LENGTH){
				m_fout << endl;
				count = 1;
			}
			else{
				count ++;
			}
		}

		if(count != 0){
			m_fout << endl;
		}

		m_fout << endl;
	}
}

void DNAMol::export_fasta_protein(ofstream &m_fout, const vector<int> &m_gi)
{
	list<GeneAnnotation>::const_iterator iter;
	
	for(iter = gene_list.begin();iter != gene_list.end();iter++){
		
		// Skip genes that do not also have CDS records
		//if( (iter->type() == GeneAnnotation::GENE) && !iter->gene_has_cds() ){
		//	continue;
		//}
		
		// Is this gi in the list?
		const int gi = iter->gi();
		vector<int>::const_iterator gi_iter = lower_bound(m_gi.begin(), m_gi.end(), gi);
		
		if( !( ( gi_iter != m_gi.end() ) && (gi == *gi_iter) ) ){
			continue;
		}
		
		m_fout << '>' << iter->seq_id_str() << " " ;
		
		bool trim_stop_codon = true;
		
		switch( iter->type() ){
			case GeneAnnotation::CDS:
				m_fout << "CDS ";
				trim_stop_codon = true;
				break;
			case GeneAnnotation::GENE:
				m_fout << "GENE ";
				trim_stop_codon = true;
				break;
			case GeneAnnotation::PSEUDO_GENE:
				m_fout << "PSEUDO-GENE ";
				trim_stop_codon = false;
				break;
			case GeneAnnotation::RNA:
				m_fout << "RNA ";
				trim_stop_codon = false;
				break;
			case GeneAnnotation::tRNA:
				m_fout << "tRNA ";
				trim_stop_codon = false;
				break;
			case GeneAnnotation::NONE:
				m_fout << "Intergenic space ";
				trim_stop_codon = false;
				break;
			case GeneAnnotation::PRIMER:
				m_fout << "PCR Primer ";
				trim_stop_codon = false;
				break;
			case GeneAnnotation::TFBS:
				m_fout << "Transcription factor binding site ";
				trim_stop_codon = false;
				break;
			case GeneAnnotation::USER:
				m_fout << "User defined ";
				trim_stop_codon = false;
				break;
			default:
				m_fout << "Misc ";
				trim_stop_codon = false;
				break;
		};

		if(iter->info(GeneAnnotation::LOCUS) != ""){
			m_fout << iter->info(GeneAnnotation::LOCUS) << " ";
		}
		else{
			if(iter->info(GeneAnnotation::LOCUS_TAG) != ""){
				m_fout << iter->info(GeneAnnotation::LOCUS_TAG) << " ";
			}
		}
		
		if(iter->info(GeneAnnotation::PRODUCT) != ""){
			m_fout << iter->info(GeneAnnotation::PRODUCT);
		}

		if(iter->info(GeneAnnotation::EC) != ""){
			m_fout << iter->info(GeneAnnotation::EC);
		}

		m_fout << endl;
		
		// Note that subseq automatically returns the DNA sequence in
		// the correct frame (and handles segmented genes too)
		DNA3Seq sub_seq = iter->subseq( seq, iter->cds_start(), iter->cds_stop() );
		
		const string protein_seq = dna_to_protein(sub_seq, 0, trim_stop_codon);
		
		string::const_iterator iter;
		unsigned int count = 1;

		for(iter = protein_seq.begin();iter != protein_seq.end();iter++){
			m_fout << *iter;

			if(count == FASTA_LINE_LENGTH){
				m_fout << endl;
				count = 1;
			}
			else{
				count ++;
			}
		}

		if(count != 0){
			m_fout << endl;
		}

		m_fout << endl;
	}
}

void DNAMol::export_ptt(ofstream &m_fout, const list<unsigned int> &m_annot)
{
	// The PTT file format requires the number of "proteins" in the header
	unsigned int num_annot = 0;
	
	// Create a fast lookup table for annotations to export
	vector<bool> annot_to_export(GeneAnnotation::LAST_ANNOT);
	
	list<unsigned int>::const_iterator annot_iter;
	
	for(annot_iter = m_annot.begin();annot_iter != m_annot.end();annot_iter++){
	
		if(*annot_iter >= GeneAnnotation::LAST_ANNOT){
			throw "Export annotation out of bounds!";
		}
		
		annot_to_export[*annot_iter] = true;
	}
	
	list<GeneAnnotation>::const_iterator iter;
	
	for(iter = gene_list.begin();iter != gene_list.end();iter++){
		if(annot_to_export[iter->type()] == true){
			num_annot ++;
		}
	}
	
	if(info(DNAMol::TAXA_NAME) != ""){
		m_fout << info(DNAMol::TAXA_NAME) << " ";
	}
	else{
		if(info(DNAMol::GENUS) != ""){
			m_fout << info(DNAMol::GENUS) << " ";
		}
		
		if(info(DNAMol::SPECIES) != ""){
			m_fout << info(DNAMol::SPECIES) << " ";
		}
		
		if(info(DNAMol::SUBSPECIES) != ""){
			m_fout << info(DNAMol::SUBSPECIES) << " ";
		}
	}
	
	if(info(DNAMol::SOURCE) != ""){
		m_fout << "," << info(DNAMol::SOURCE);
	}
	
	const unsigned int seq_len =  seq.size();
	
	// How many bases in this sequence?
	m_fout << " 0.." << seq_len << endl;
	
	m_fout << num_annot << " proteins" << endl;
	
	m_fout << "Location	Strand\tLength\tPID\tGene\tSynonym\tCode\tCOG\tProduct" << endl;
	
	for(iter = gene_list.begin();iter != gene_list.end();iter++){
		if(annot_to_export[iter->type()] == false){
			continue;
		}
		
		// Location (1's based)
		m_fout << iter->start() + 1 << ".." << iter->stop() + 1<< '\t';
		
		// Strand
		if(iter->is_complement() == true){
			m_fout << "-\t";
		}
		else{
			m_fout << "+\t";
		}
		
		// Length
		m_fout << iter->length( seq_len ) << '\t';
		
		// PID
		const int gi = iter->gi();
		
		if(gi > 0){
			m_fout << gi << '\t';
		}
		else{
			m_fout << "-\t";
		}
		
		// Gene
		if(iter->info(GeneAnnotation::LOCUS) != ""){
			m_fout << iter->info(GeneAnnotation::LOCUS) << '\t';
		}
		else{
			m_fout << "-\t";
		}
		
		// Synonym
		if(iter->info(GeneAnnotation::LOCUS_TAG) != ""){
			m_fout << iter->info(GeneAnnotation::LOCUS_TAG) << '\t';
		}
		else{
			m_fout << "-\t";
		}
		
		// Code (COG)
		if(iter->info(GeneAnnotation::COG_CODE) != ""){
			m_fout << iter->info(GeneAnnotation::COG_CODE) << '\t';
		}
		else{
			m_fout << "-\t";
		}
		
		// COG ID
		if(iter->info(GeneAnnotation::COG_ID) != ""){
			m_fout << iter->info(GeneAnnotation::COG_ID) << '\t';
		}
		else{
			m_fout << "-\t";
		}
		
		// Product
		m_fout << iter->info(GeneAnnotation::PRODUCT) << endl;
	}
}

void DNAMol::export_ptt(ofstream &m_fout, const vector<int> &m_gi)
{
	// The PTT file format requires the number of "proteins" in the header
	unsigned int num_annot = 0;
	
	list<GeneAnnotation>::const_iterator iter;
	
	for(iter = gene_list.begin();iter != gene_list.end();iter++){
		
		// Is this gi in the list?
		const int gi = iter->gi();
		vector<int>::const_iterator gi_iter = lower_bound(m_gi.begin(), m_gi.end(), gi);
		
		if( !( ( gi_iter != m_gi.end() ) && (gi == *gi_iter) ) ){
			continue;
		}
		
		num_annot ++;
	}
	
	if(info(DNAMol::TAXA_NAME) != ""){
		m_fout << info(DNAMol::TAXA_NAME) << " ";
	}
	else{
		if(info(DNAMol::GENUS) != ""){
			m_fout << info(DNAMol::GENUS) << " ";
		}
		
		if(info(DNAMol::SPECIES) != ""){
			m_fout << info(DNAMol::SPECIES) << " ";
		}
		
		if(info(DNAMol::SUBSPECIES) != ""){
			m_fout << info(DNAMol::SUBSPECIES) << " ";
		}
	}
	
	if(info(DNAMol::SOURCE) != ""){
		m_fout << "," << info(DNAMol::SOURCE);
	}
	
	const unsigned int seq_len =  seq.size();
	
	// How many bases in this sequence?
	m_fout << " 0.." << seq_len << endl;
	
	m_fout << num_annot << " proteins" << endl;
	
	m_fout << "Location	Strand\tLength\tPID\tGene\tSynonym\tCode\tCOG\tProduct" << endl;
	
	for(iter = gene_list.begin();iter != gene_list.end();iter++){
	
		// Is this gi in the list?
		const int gi = iter->gi();
		vector<int>::const_iterator gi_iter = lower_bound(m_gi.begin(), m_gi.end(), gi);
		
		if( !( ( gi_iter != m_gi.end() ) && (gi == *gi_iter) ) ){
			continue;
		}
		
		// Location (1's based)
		m_fout << iter->start() + 1 << ".." << iter->stop() + 1<< '\t';
		
		// Strand
		if(iter->is_complement() == true){
			m_fout << "-\t";
		}
		else{
			m_fout << "+\t";
		}
		
		// Length
		m_fout << iter->length( seq_len ) << '\t';
		
		if(gi > 0){
			m_fout << gi << '\t';
		}
		else{
			m_fout << "-\t";
		}
		
		// Gene
		if(iter->info(GeneAnnotation::LOCUS) != ""){
			m_fout << iter->info(GeneAnnotation::LOCUS) << '\t';
		}
		else{
			m_fout << "-\t";
		}
		
		// Synonym
		if(iter->info(GeneAnnotation::LOCUS_TAG) != ""){
			m_fout << iter->info(GeneAnnotation::LOCUS_TAG) << '\t';
		}
		else{
			m_fout << "-\t";
		}
		
		// Code (COG)
		if(iter->info(GeneAnnotation::COG_CODE) != ""){
			m_fout << iter->info(GeneAnnotation::COG_CODE) << '\t';
		}
		else{
			m_fout << "-\t";
		}
		
		// COG ID
		if(iter->info(GeneAnnotation::COG_ID) != ""){
			m_fout << iter->info(GeneAnnotation::COG_ID) << '\t';
		}
		else{
			m_fout << "-\t";
		}
		
		// Product
		m_fout << iter->info(GeneAnnotation::PRODUCT) << endl;
	}
}

void DNAMol::export_glimmer(ofstream &m_fout, const list<unsigned int> &m_annot)
{
	// Create a fast lookup table for annotations to export
	vector<bool> annot_to_export(GeneAnnotation::LAST_ANNOT);
	
	list<unsigned int>::const_iterator annot_iter;
	
	for(annot_iter = m_annot.begin();annot_iter != m_annot.end();annot_iter++){
	
		if(*annot_iter >= GeneAnnotation::LAST_ANNOT){
			throw "Export annotation out of bounds!";
		}
		
		annot_to_export[*annot_iter] = true;
	}
	
	list<GeneAnnotation>::const_iterator iter;
	
	m_fout << '>' << seq_id_str() << ' ';
	
	if(info(DNAMol::TAXA_NAME) != ""){
		m_fout << info(DNAMol::TAXA_NAME) << " ";
	}
	else{
		if(info(DNAMol::GENUS) != ""){
			m_fout << info(DNAMol::GENUS) << " ";
		}
		
		if(info(DNAMol::SPECIES) != ""){
			m_fout << info(DNAMol::SPECIES) << " ";
		}
		
		if(info(DNAMol::SUBSPECIES) != ""){
			m_fout << info(DNAMol::SUBSPECIES) << " ";
		}
	}
	
	m_fout << endl;
		
	unsigned int locus_index = 1;
	const string locus_name = "Orf";
	
	for(iter = gene_list.begin();iter != gene_list.end();iter++){
	
		if(annot_to_export[iter->type()] == false){
			continue;
		}
		
		// Gene name
		if(iter->info(GeneAnnotation::LOCUS) != ""){
			m_fout << iter->info(GeneAnnotation::LOCUS) << '\t';
		}
		else{
			if(iter->info(GeneAnnotation::LOCUS_TAG) != ""){
				m_fout << iter->info(GeneAnnotation::LOCUS_TAG) << '\t';
			}
			else{
				m_fout << locus_name << locus_index++ << "\t";
			}
		}
		
		// Strand
		if(iter->is_complement() == true){
			// Location (1's based)
			m_fout << iter->stop() + 1 << '\t' << iter->start() + 1 << "\t-1" << endl;
		}
		else{
			// Location (1's based)
			m_fout << iter->start() + 1 << '\t' << iter->stop() + 1 << "\t+1" << endl;
		}
	}
}

void DNAMol::export_glimmer(ofstream &m_fout, const vector<int> &m_gi)
{
	list<GeneAnnotation>::const_iterator iter;
	
	m_fout << '>' << seq_id_str() << ' ';
	
	if(info(DNAMol::TAXA_NAME) != ""){
		m_fout << info(DNAMol::TAXA_NAME) << " ";
	}
	else{
		if(info(DNAMol::GENUS) != ""){
			m_fout << info(DNAMol::GENUS) << " ";
		}
		
		if(info(DNAMol::SPECIES) != ""){
			m_fout << info(DNAMol::SPECIES) << " ";
		}
		
		if(info(DNAMol::SUBSPECIES) != ""){
			m_fout << info(DNAMol::SUBSPECIES) << " ";
		}
	}
	
	m_fout << endl;
		
	unsigned int locus_index = 1;
	const string locus_name = "Orf";
	
	for(iter = gene_list.begin();iter != gene_list.end();iter++){
	
		// Is this gi in the list?
		const int gi = iter->gi();
		vector<int>::const_iterator gi_iter = lower_bound(m_gi.begin(), m_gi.end(), gi);
		
		if( !( ( gi_iter != m_gi.end() ) && (gi == *gi_iter) ) ){
			continue;
		}
		
		// Gene name
		if(iter->info(GeneAnnotation::LOCUS) != ""){
			m_fout << iter->info(GeneAnnotation::LOCUS) << '\t';
		}
		else{
			if(iter->info(GeneAnnotation::LOCUS_TAG) != ""){
				m_fout << iter->info(GeneAnnotation::LOCUS_TAG) << '\t';
			}
			else{
				m_fout << locus_name << locus_index++ << "\t";
			}
		}
		
		// Strand
		if(iter->is_complement() == true){
			// Location (1's based)
			m_fout << iter->stop() + 1 << '\t' << iter->start() + 1 << "\t-1" << endl;
		}
		else{
			// Location (1's based)
			m_fout << iter->start() + 1 << '\t' << iter->stop() + 1 << "\t+1" << endl;
		}
	}
}

void DNAMol::export_gbk(ofstream &m_fout, const list<unsigned int> &m_annot)
{
	// Create a fast lookup table for annotations to export
	vector<bool> annot_to_export(GeneAnnotation::LAST_ANNOT);
	
	list<unsigned int>::const_iterator annot_iter;
	
	for(annot_iter = m_annot.begin();annot_iter != m_annot.end();annot_iter++){
	
		if(*annot_iter >= GeneAnnotation::LAST_ANNOT){
			throw "Export annotation out of bounds!";
		}
		
		annot_to_export[*annot_iter] = true;
	}
	
	// LOCUS
	m_fout << "LOCUS       " << accession(false /*no version*/) 
		  << ' '
		  << seq.size() 
		  << " bp    DNA     "
		  << (circular() ? "circular" : "linear")
		  << endl;
	
	// Definition
	m_fout << "DEFINITION  "
		  << info(TAXA_NAME) 
		  << ' ' << info(SOURCE) << endl;
		  
	// Accession
	m_fout << "ACCESSION   " << accession(false /*no version*/) << endl;
	
	// Version
	m_fout << "VERSION     " << accession(true /*version*/);
	
	if(gi() > 0){
		m_fout << " GI:" << gi();
	}
	
	m_fout << endl;
	
	// Leave KEYWORDS empty for now
	m_fout << "KEYWORDS    ." << endl;
	
	// Source
	m_fout << "SOURCE      " << info(TAXA_NAME) << endl;
	
	// Organism
	m_fout << "  ORGANISM  " << info(TAXA_NAME) << endl;
	
	m_fout << "FEATURES             Location/Qualifiers" << endl;
	
	m_fout << "     source          " << "1.." 
		  << seq.size() << endl;
	
	format_gbk_record(m_fout, "organism", info(TAXA_NAME), true);

	// The annotation records
	list<GeneAnnotation>::const_iterator iter;
	
	for(iter = gene_list.begin();iter != gene_list.end();iter++){
		if(annot_to_export[iter->type()] == false){
			continue;
		}
		
		iter->write_gbk(m_fout, seq);
	}
	
	// Write the sequence data
	m_fout << "ORIGIN      ";
	
	// The sequence data is organized into 6 blocks of 10 characters
	DNA3Seq::iterator seq_iter;
	
	unsigned int base_index = 0;
	
	for(seq_iter = seq.begin();seq_iter != seq.end();seq_iter++, base_index++){
		if( (base_index % 60) == 0){
			
			m_fout << endl;
			
			const string index = itoa(base_index + 1);
			const int len = index.size();
			
			for(int i = len;i < 9;i++){
				m_fout << ' ';
			}
			
			m_fout << index;
		}
	
		if( (base_index % 10) == 0){
			m_fout << ' ';
		}
		
		m_fout << (char)tolower( seq_iter.c_base() );
	}
	
	// Terminate this GBK record
	m_fout << "\n//" << endl;
}

void GeneAnnotation::write_gbk(std::ofstream &m_fout, const DNA3Seq &m_seq) const
{
	switch(gene_type){
		case CDS:
			write_CDS_gbk(m_fout, m_seq);
			break;
		case GENE:
			write_GENE_gbk(m_fout, m_seq);
			break;
		case PSEUDO_GENE:
			write_PSEUDO_GENE_gbk(m_fout);
			break;
		case RNA:
			write_RNA_gbk(m_fout);
			break;
		case tRNA:
			write_tRNA_gbk(m_fout);
			break;
		case IMP:
			write_IMP_gbk(m_fout);
			break;
		case PRIMER:
			write_PRIMER_gbk(m_fout);
			break;
		case TFBS:
			write_TFBS_gbk(m_fout);
			break;
		case USER:
			write_USER_gbk(m_fout);
			break;
		case NONE:
			write_NONE_gbk(m_fout);
			break;
		default:
			throw __FILE__ ":GeneAnnotation::write_gbk: Uknown annotation type";
	};
}

void GeneAnnotation::write_CDS_gbk(ofstream &m_fout, const DNA3Seq &m_seq) const
{
	m_fout << "     CDS             ";
	write_seg_GBK(m_fout);
	m_fout << endl;
	
	if(info(LOCUS) != ""){
		format_gbk_record(m_fout, "gene", info(LOCUS), true);
	}
	
	if(info(LOCUS_TAG) != ""){
		format_gbk_record(m_fout, "locus_tag", info(LOCUS_TAG), true);
	}
	
	if(info(NOTE) != ""){
		format_gbk_record(m_fout, "note", info(NOTE), true);
	}
	
	if(info(PRODUCT) != ""){
		format_gbk_record(m_fout, "product", info(PRODUCT), true);
	}
	
	if(gi() > 0){
		format_gbk_record(m_fout, "db_xref", "GI:" + itoa( gi() ), true);
	}
	
	if(is_custom_color() == true){
		
		const int red = (int)(255.0f*color()[0]);
		const int green = (int)(255.0f*color()[1]);
		const int blue = (int)(255.0f*color()[2]);
		const int alpha = (int)(255.0f*color()[3]);
		
		format_gbk_record(m_fout, "custom_color", "RGBA:" + 
			itoa(red) + "," + 
			itoa(green) + "," + 
			itoa(blue) + "," + 
			itoa(alpha), true);
	}
	
	const string acc = accession(true);
	
	if(acc != ""){
		format_gbk_record(m_fout, "protein_id", acc, true);
	}
	
	// Note that subseq automatically returns the DNA sequence in
	// the correct frame (and handles segmented genes too)
	DNA3Seq sub_seq = subseq( m_seq, cds_start(), cds_stop() );

	const string protein_seq = dna_to_protein(sub_seq, 0, true /* Trim the stop codon */);
		
	format_gbk_record(m_fout, "translation", protein_seq, true, false /* Don't look for word boundaries */);
}

void GeneAnnotation::write_GENE_gbk(ofstream &m_fout, const DNA3Seq &m_seq) const
{
	m_fout << "     gene            ";
	//write_seg_GBK(m_fout);
	// Write the segment bounds
	if(is_complement() == true){
		m_fout << "complement("
			  << gene_start + 1 << ".." << gene_stop + 1
			  << ")" << endl;
	}
	else{
		m_fout << gene_start + 1 << ".." << gene_stop + 1 << endl;
	}
	
	if(info(LOCUS) != ""){
		format_gbk_record(m_fout, "gene", info(LOCUS), true);
	}
	
	if(info(LOCUS_TAG) != ""){
		format_gbk_record(m_fout, "locus_tag", info(LOCUS_TAG), true);
	}
	
	// Follow the gene record with a CDS record
	write_CDS_gbk(m_fout, m_seq);
}

void GeneAnnotation::write_PSEUDO_GENE_gbk(ofstream &m_fout) const
{
	m_fout << "     gene            ";
	write_seg_GBK(m_fout);
	m_fout << endl;
	
	if(info(LOCUS) != ""){
		format_gbk_record(m_fout, "gene", info(LOCUS), true);
	}
	
	if(info(LOCUS_TAG) != ""){
		format_gbk_record(m_fout, "locus_tag", info(LOCUS_TAG), true);
	}
	
	if(is_custom_color() == true){
		
		const int red = (int)(255.0f*color()[0]);
		const int green = (int)(255.0f*color()[1]);
		const int blue = (int)(255.0f*color()[2]);
		const int alpha = (int)(255.0f*color()[3]);
		
		format_gbk_record(m_fout, "custom_color", "RGBA:" + 
			itoa(red) + "," + 
			itoa(green) + "," + 
			itoa(blue) + "," + 
			itoa(alpha), true);
	}
	
	m_fout << "                     /pseudo" << endl;
}

void GeneAnnotation::write_RNA_gbk(ofstream &m_fout) const
{
	m_fout << "     rRNA            ";
	write_seg_GBK(m_fout);
	m_fout << endl;
	
	if(info(LOCUS) != ""){
		format_gbk_record(m_fout, "gene", info(LOCUS), true);
	}
	
	if(info(LOCUS_TAG) != ""){
		format_gbk_record(m_fout, "locus_tag", info(LOCUS_TAG), true);
	}
	
	if(is_custom_color() == true){
		
		const int red = (int)(255.0f*color()[0]);
		const int green = (int)(255.0f*color()[1]);
		const int blue = (int)(255.0f*color()[2]);
		const int alpha = (int)(255.0f*color()[3]);
		
		format_gbk_record(m_fout, "custom_color", "RGBA:" + 
			itoa(red) + "," + 
			itoa(green) + "," + 
			itoa(blue) + "," + 
			itoa(alpha), true);
	}
}

void GeneAnnotation::write_tRNA_gbk(ofstream &m_fout) const
{
	m_fout << "     tRNA            ";
	write_seg_GBK(m_fout);
	m_fout << endl;
	
	if(info(LOCUS) != ""){
		format_gbk_record(m_fout, "gene", info(LOCUS), true);
	}
	
	if(info(LOCUS_TAG) != ""){
		format_gbk_record(m_fout, "locus_tag", info(LOCUS_TAG), true);
	}
	
	if(is_custom_color() == true){
		
		const int red = (int)(255.0f*color()[0]);
		const int green = (int)(255.0f*color()[1]);
		const int blue = (int)(255.0f*color()[2]);
		const int alpha = (int)(255.0f*color()[3]);
		
		format_gbk_record(m_fout, "custom_color", "RGBA:" + 
			itoa(red) + "," + 
			itoa(green) + "," + 
			itoa(blue) + "," + 
			itoa(alpha), true);
	}
}

void GeneAnnotation::write_IMP_gbk(ofstream &m_fout) const
{
	m_fout << "     misc_feature    ";
	write_seg_GBK(m_fout);
	m_fout << endl;
	
	if(info(LOCUS) != ""){
		format_gbk_record(m_fout, "gene", info(LOCUS), true);
	}
	
	if(info(LOCUS_TAG) != ""){
		format_gbk_record(m_fout, "locus_tag", info(LOCUS_TAG), true);
	}
	
	if(info(NOTE) != ""){
		format_gbk_record(m_fout, "note", info(NOTE), true);
	}
	
	if(info(PRODUCT) != ""){
		format_gbk_record(m_fout, "product", info(PRODUCT), true);
	}
	
	if(is_custom_color() == true){
		
		const int red = (int)(255.0f*color()[0]);
		const int green = (int)(255.0f*color()[1]);
		const int blue = (int)(255.0f*color()[2]);
		const int alpha = (int)(255.0f*color()[3]);
		
		format_gbk_record(m_fout, "custom_color", "RGBA:" + 
			itoa(red) + "," + 
			itoa(green) + "," + 
			itoa(blue) + "," + 
			itoa(alpha), true);
	}
}

void GeneAnnotation::write_PRIMER_gbk(ofstream &m_fout) const
{
	m_fout << "     primer       ";
	write_seg_GBK(m_fout);
	m_fout << endl;

	format_gbk_record(m_fout, "tm", info(PRIMER_TM), true);
	format_gbk_record(m_fout, "gc", info(PRIMER_GC), true);
	format_gbk_record(m_fout, "hairpin_tm", info(PRIMER_HAIRPIN_TM), true);
	format_gbk_record(m_fout, "dimer_tm", info(PRIMER_DIMER_TM), true);
	
	if(is_custom_color() == true){
		
		const int red = (int)(255.0f*color()[0]);
		const int green = (int)(255.0f*color()[1]);
		const int blue = (int)(255.0f*color()[2]);
		const int alpha = (int)(255.0f*color()[3]);
		
		format_gbk_record(m_fout, "custom_color", "RGBA:" + 
			itoa(red) + "," + 
			itoa(green) + "," + 
			itoa(blue) + "," + 
			itoa(alpha), true);
	}
}

void GeneAnnotation::write_TFBS_gbk(ofstream &m_fout) const
{
	m_fout << "     TFBS            ";
	write_seg_GBK(m_fout);
	m_fout << endl;
	
	if(info(LOCUS_TAG) != ""){
		format_gbk_record(m_fout, "locus_tag", info(LOCUS_TAG), true);
	}
	
	if(info(NOTE) != ""){
		format_gbk_record(m_fout, "note", info(NOTE), true);
	}
	
	if(info(PRODUCT) != ""){
		format_gbk_record(m_fout, "product", info(PRODUCT), true);
	}
	
	if(is_custom_color() == true){
		
		const int red = (int)(255.0f*color()[0]);
		const int green = (int)(255.0f*color()[1]);
		const int blue = (int)(255.0f*color()[2]);
		const int alpha = (int)(255.0f*color()[3]);
		
		format_gbk_record(m_fout, "custom_color", "RGBA:" + 
			itoa(red) + "," + 
			itoa(green) + "," + 
			itoa(blue) + "," + 
			itoa(alpha), true);
	}
}

void GeneAnnotation::write_USER_gbk(ofstream &m_fout) const
{
	m_fout << "     USER            ";
	write_seg_GBK(m_fout);
	m_fout << endl;
	
	if(info(LOCUS) != ""){
		format_gbk_record(m_fout, "gene", info(LOCUS), true);
	}
	
	if(info(LOCUS_TAG) != ""){
		format_gbk_record(m_fout, "locus_tag", info(LOCUS_TAG), true);
	}
	
	if(info(NOTE) != ""){
		format_gbk_record(m_fout, "note", info(NOTE), true);
	}
	
	if(info(PRODUCT) != ""){
		format_gbk_record(m_fout, "product", info(PRODUCT), true);
	}
	
	if(is_custom_color() == true){
		
		const int red = (int)(255.0f*color()[0]);
		const int green = (int)(255.0f*color()[1]);
		const int blue = (int)(255.0f*color()[2]);
		const int alpha = (int)(255.0f*color()[3]);
		
		format_gbk_record(m_fout, "custom_color", "RGBA:" + 
			itoa(red) + "," + 
			itoa(green) + "," + 
			itoa(blue) + "," + 
			itoa(alpha), true);
	}
}

void GeneAnnotation::write_NONE_gbk(ofstream &m_fout) const
{
	// Since the gbk format does not have a record for intergenic space,
	// write this record as a misc_feature.
	write_IMP_gbk(m_fout);
}

void GeneAnnotation::write_seg_GBK(ofstream &m_fout) const
{
	if(is_complement() == true){
		m_fout << "complement(";
	}
	
	// Segment lists are 1's based
	switch( seg_list.size() ){
		case 0:
			m_fout << gene_start + 1 << ".." << gene_stop + 1;
			break;
		case 1:
			m_fout << seg_list.front().first + 1 << ".." << seg_list.front().second + 1;
			break;
		default:
			{
				m_fout << "join(";

				list< pair<unsigned int, unsigned int> >::const_iterator iter = seg_list.begin();

				// Segment lists are 1's based
				while( iter != seg_list.end() ){
					m_fout << iter->first + 1 << ".." << iter->second + 1;

					iter ++;

					if( iter != seg_list.end() ){
						m_fout << ',';
					}

				}

				m_fout << ")";
			}
			
			break;
	};
	
	if(is_complement() == true){
		m_fout << ")";
	}
}

string itoa(const int &m_int)
{
	stringstream ss;
	
	ss << m_int;

	return ss.str();
}

void format_gbk_record(ofstream &m_fout, const string &m_name, const string &m_value, 
	const bool &m_quote_value, const bool &m_no_word_breaks /*= true */)
{
	const string prefix(GBK_LINE_PREFIX, ' ');

	stringstream ss_out;
	
	ss_out << '/' << m_name << '=';
	
	if(m_quote_value){
		ss_out << '"';
	}
	
	ss_out << m_value;
	
	if(m_quote_value){
		ss_out << '"';
	}
	
	unsigned int count = 0;
	
	m_fout << prefix;
	
	if(m_no_word_breaks){

		stringstream ss_in( ss_out.str() );
		string buffer;

		while(ss_in >> buffer){
			
			const unsigned int len = buffer.size() 
			+ ( (count != 0) ? 1 : 0 /* word separator */);
			
			if( (count != 0) && ( ( count + len ) >= GBK_LINE_LENGTH) ){
				m_fout << endl << prefix;
				count = 0;
			}
			
			if(count != 0){
				m_fout << ' ';
			}
			
			m_fout << buffer;

			count += len;
		}
	}
	else{
		string::const_iterator iter;
		
		const string buffer = ss_out.str();
		
		for(iter = buffer.begin();iter != buffer.end();iter ++){

			if(count == GBK_LINE_LENGTH){
				m_fout << '\n' << prefix;
				count = 0;
			}

			m_fout << *iter;

			count++;
		}
	}
	
	m_fout << endl;
}
