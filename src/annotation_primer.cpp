// This program was prepared by the Regents of the University of California at 
// Los Alamos National Laboratory (the University) under contract No. W-7405-ENG-36 
// with the U.S. Department of Energy (DOE). All rights in the program are reserved 
// by the DOE and the University.  Permission is granted to the public to copy and 
// use this software without charge, provided that this Notice and any statement of 
// authorship are reproduced on all copies.  Neither the U.S. Government nor the 
// University makes any warranty, express or implied, or assumes any liability or 
// responsibility for the use of this software.

#include "stdafx.h"
#include "annotation.h"
#include "nuc_cruc.h"
#include "primer.h"

using namespace std;

void DNAMol::compute_primers(const unsigned int &m_options, const unsigned int &m_len,
							 const unsigned int &m_run,
							 const float &m_salt, const float &m_strand,
							 const float &m_min_gc, const float &m_max_gc, 
							 const float &m_tm, const float &m_mis_prime_tm)
{
	
	// Prepare the hybridization engine
	NucCruc melt_plus;
	NucCruc melt_minus;

	melt_plus.Salt(m_salt);
	melt_plus.Strand(m_strand);

	melt_minus.Salt(m_salt);
	melt_minus.Strand(m_strand);

	// Prepare the heuristic engines for both forward (+) 
	// and reverse (-) strands
	ASSY_HEURISTIC::PCRPrimer pcr_plus(m_options, m_min_gc, m_max_gc);
	ASSY_HEURISTIC::PCRPrimer pcr_minus(m_options, m_min_gc, m_max_gc);

	// Set the run length
	pcr_plus.run(m_run);
	pcr_minus.run(m_run);

	// Scan through the sequence for suitable binding sites
	DNA3Seq::iterator iter;
	
	// Assign every primer a unique number
	unsigned int primer_id = 1;

	GeneAnnotation primer_plus;
	GeneAnnotation primer_minus;

	float primer_plus_score = 0.0;
	float primer_minus_score = 0.0;

	for(iter = seq.begin();iter != seq.end();iter ++){
		switch(iter.base()){
			case DNA3Seq::A:
				pcr_plus.push_back_native(ASSY_HEURISTIC::PCRPrimer::A);
				melt_plus.push_back_native(NucCruc::A);

				pcr_minus.push_front_native(ASSY_HEURISTIC::PCRPrimer::T);
				melt_minus.push_front_native(NucCruc::T);
				break;
			case DNA3Seq::T:
				pcr_plus.push_back_native(ASSY_HEURISTIC::PCRPrimer::T);
				melt_plus.push_back_native(NucCruc::T);

				pcr_minus.push_front_native(ASSY_HEURISTIC::PCRPrimer::A);
				melt_minus.push_front_native(NucCruc::A);
				break;
			case DNA3Seq::G:
				pcr_plus.push_back_native(ASSY_HEURISTIC::PCRPrimer::G);
				melt_plus.push_back_native(NucCruc::G);

				pcr_minus.push_front_native(ASSY_HEURISTIC::PCRPrimer::C);
				melt_minus.push_front_native(NucCruc::C);
				break;
			case DNA3Seq::C:
				pcr_plus.push_back_native(ASSY_HEURISTIC::PCRPrimer::C);
				melt_plus.push_back_native(NucCruc::C);

				pcr_minus.push_front_native(ASSY_HEURISTIC::PCRPrimer::G);
				melt_minus.push_front_native(NucCruc::G);
				break;
			default:
				// Illegal base
				pcr_plus.clear();
				melt_plus.clear();

				pcr_minus.clear();
				melt_minus.clear();
				break;

		};
		
		// Do we have enough bases to test?
		if(pcr_plus.size() == m_len){
			
			// Note that tm and gc will be the same for both plus and
			// minus strands, so only compute it once.
			bool update_tm = true;
			bool update_gc = true;
			float tm;
			float gc;

			// Test the plus strand primer
			if(pcr_plus() == PCR_VALID){	
				tm = melt_plus.Tm();
				update_tm = false;

				if(tm > m_tm){
					const float dimer_tm = melt_plus.MaxDimerTm();
					
					if(dimer_tm < m_mis_prime_tm){
						// We found a match! Add a primer annotation
						
						const unsigned int primer_stop = iter.index();
						const unsigned int primer_start = primer_stop - (m_len - 1);
						
						// Is this the first + primer we've found?
						if(primer_plus.type() == GeneAnnotation::NONE){
							primer_plus.type(GeneAnnotation::PRIMER);
							primer_plus.strand(Seq_strand_plus);
							primer_plus.start(primer_start);
							primer_plus.stop(primer_stop);
							
							primer_plus.primer_id(primer_id);
							primer_plus.primer_tm(tm);
							//primer_plus.primer_hairpin_tm(hairpin_tm);
							primer_plus.primer_dimer_tm(dimer_tm);
							
							
							primer_plus.primer_gc( gc = melt_plus.gc_content() );
							update_gc = false;
							
							primer_id++;
							
							// Save the primer score
							//primer_plus_score = tm - max(hairpin_tm, dimer_tm);
							primer_plus_score = tm - dimer_tm;
							
						} else if(primer_plus.stop() < primer_start){
							// Save the last + primer
							gene_list.push_back(primer_plus);
							
							// Increment the primer id
							primer_id++;
							
							primer_plus.type(GeneAnnotation::PRIMER);
							primer_plus.strand(Seq_strand_plus);
							primer_plus.start(primer_start);
							primer_plus.stop(primer_stop);
							
							primer_plus.primer_id(primer_id);
							primer_plus.primer_tm(tm);
							//primer_plus.primer_hairpin_tm(hairpin_tm);
							primer_plus.primer_dimer_tm(dimer_tm);
							
							primer_plus.primer_gc( gc = melt_plus.gc_content() );
							update_gc = false;
							
							// Save the primer score
							//primer_plus_score = tm - max(hairpin_tm, dimer_tm);
							primer_plus_score = tm - dimer_tm;
						} else{
							// Do we keep the current primer (stored in primer_plus) or
							// replace it with this new, but overlapping, primer?
							//const float tmp_score = tm - max(hairpin_tm, dimer_tm);
							const float tmp_score = tm - dimer_tm;
							
							if(tmp_score > primer_plus_score){
								// We found a better scoring primer!
								primer_plus.type(GeneAnnotation::PRIMER);
								primer_plus.strand(Seq_strand_plus);
								primer_plus.start(primer_start);
								primer_plus.stop(primer_stop);
								
								primer_plus.primer_id(primer_id);
								primer_plus.primer_tm(tm);
								//primer_plus.primer_hairpin_tm(hairpin_tm);
								primer_plus.primer_dimer_tm(dimer_tm);
								
								primer_plus.primer_gc( gc = melt_plus.gc_content() );
								update_gc = false;
								
								// Save the primer score
								primer_plus_score = tmp_score;
							}
						}
					}
				}
			}

			// Test the minus strand primer
			if(pcr_minus() == PCR_VALID){
				
				if(update_tm){
					tm = melt_minus.Tm();
				}

				if(tm > m_tm){
					const float dimer_tm = melt_minus.MaxDimerTm();
					
					if(dimer_tm < m_mis_prime_tm){
						// We found a match!
						const unsigned int primer_stop = iter.index();
						const unsigned int primer_start = primer_stop - (m_len - 1);
						
						// Is this the first + primer we've found?
						if(primer_minus.type() == GeneAnnotation::NONE){
							primer_minus.type(GeneAnnotation::PRIMER);
							primer_minus.strand(Seq_strand_minus);
							primer_minus.start(primer_start);
							primer_minus.stop(primer_stop);
							
							primer_minus.primer_id(primer_id);
							primer_minus.primer_tm(tm);
							//primer_minus.primer_hairpin_tm(hairpin_tm);
							primer_minus.primer_dimer_tm(dimer_tm);
							
							if(update_gc){
								primer_minus.primer_gc( melt_minus.gc_content() );
							}
							else{
								primer_minus.primer_gc(gc);
							}
							
							primer_id++;
							
							// Save the primer score
							//primer_minus_score = tm - max(hairpin_tm, dimer_tm);
							primer_minus_score = tm - dimer_tm;
							
						} else if(primer_minus.stop() < primer_start){
							// Save the last + primer
							gene_list.push_back(primer_minus);
							
							// Increment the primer id
							primer_id++;
							
							primer_minus.type(GeneAnnotation::PRIMER);
							primer_minus.strand(Seq_strand_minus);
							primer_minus.start(primer_start);
							primer_minus.stop(primer_stop);
							
							primer_minus.primer_id(primer_id);
							primer_minus.primer_tm(tm);
							//primer_minus.primer_hairpin_tm(hairpin_tm);
							primer_minus.primer_dimer_tm(dimer_tm);
							
							if(update_gc){
								primer_minus.primer_gc( melt_minus.gc_content() );
							}
							else{
								primer_minus.primer_gc(gc);
							}
							
							// Save the primer score
							//primer_minus_score = tm - max(hairpin_tm, dimer_tm);
							primer_minus_score = tm - dimer_tm;
						} else{
							// Do we keep the current primer (stored in primer_minus) or
							// replace it with this new, but overlapping, primer?
							//const float tmp_score = tm - max(hairpin_tm, dimer_tm);
							const float tmp_score = tm - dimer_tm;
							
							if(tmp_score > primer_minus_score){
								// We found a better scoring primer!
								primer_minus.type(GeneAnnotation::PRIMER);
								primer_minus.strand(Seq_strand_minus);
								primer_minus.start(primer_start);
								primer_minus.stop(primer_stop);
								
								primer_minus.primer_id(primer_id);
								primer_minus.primer_tm(tm);
								//primer_minus.primer_hairpin_tm(hairpin_tm);
								primer_minus.primer_dimer_tm(dimer_tm);
								
								if(update_gc){
									primer_minus.primer_gc( melt_minus.gc_content() );
								}
								else{
									primer_minus.primer_gc(gc);
								}
								
								// Save the primer score
								primer_minus_score = tmp_score;
							}
						}
					}
				}
			}

			pcr_plus.pop_front();
			melt_plus.pop_front();

			pcr_minus.pop_back();
			melt_minus.pop_back();
		}
	}

	// Save the last primers
	if(primer_plus.type() != GeneAnnotation::NONE){
		gene_list.push_back(primer_plus);
	}

	if(primer_minus.type() != GeneAnnotation::NONE){
		gene_list.push_back(primer_minus);
	}
}


// Remove any previously computed primers
void DNAMol::clear_primers()
{
	
	// Clear any selected annotations in case the user has selected a primer
	// that is about to be removed.
	clear_select();

	// Find all annotations of type PRIMER in the current
	// annotation list
	typedef list<GeneAnnotation>::iterator I;

	list<I> reaper;

	I iter;

	for(iter = gene_list.begin();iter != gene_list.end();iter++){
		if(iter->type() == GeneAnnotation::PRIMER){
			reaper.push_back(iter);
		}
	}

	list<I>::iterator reaper_iter;

	for(reaper_iter = reaper.begin();reaper_iter != reaper.end();reaper_iter++){
		gene_list.erase(*reaper_iter);
	}

	// Note that the annotation list still need to be reprocessed. Perform this task
	// manually AFTER possible further primer computation
}
